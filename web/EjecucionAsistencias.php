<?php
	session_start();
	$usuario=$_SESSION['log_USUARIO'];
	$idtecnico =$_SESSION['log_IDTECNICO'];
	
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	$numorden= $_GET['numot'];
	
	$gestiones = $x->AsistenciasGestiones($numorden);
	
?>
	<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    	<title><?php echo(Config::$mvc_titulo); ?></title>
    	<!--  ESTILOS  -->
    	<link rel="stylesheet" type="text/css" href="css/estilo.css" />
    	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjZhArEJGrMLLfujWfelLUPyBnkvS5gQw"></script>
		<script type="text/javascript" src="js/scripts.js">
		</script> 
	</head>
	<body>

<?php
    echo '<input type="hidden" name="latitud" id="latitud" >';
    echo '<input type="hidden" name="longitud" id="longitud" >';
	$informacion= $x->DatosAsistencias($numorden);
	echo '<div class="ventana_titulo">GESTIONES';
	echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%" /></a></div>';
	echo '</div>';
	//Informacion de la orden.
	echo('<div class="info">&nbsp;<b>CLIENTE: </b>' . $informacion[0][Monitoreo] . ' - ' . substr($informacion[0][Nombre],0,35) . ' <b>OT: ' . $informacion[0][Numero] . '</div><hr width="25%">');

	if (count($gestiones)>0){
		echo('<div style="overflow:scroll;height:100%;width:100%">');
		echo('<table class="Gestiones">');
			echo('<tr>');
			echo('<th>FECHA</th>');
			echo('<th>NOTA</th>');
			echo('<th>USUARIO</th>');
			echo('</tr>');
			$css = 0; $class = '';
			foreach ($gestiones as $registro){
				if($css==1){
					$class = 'class="impar"';
					$css = 0;
				}else{
					$class = '';
					$css = 1;
				}
				echo '<tr ' . $class . '>';
				echo '<td id="GestionFecha">' . date('d-m-Y',strtotime($registro[Fecha])) . '<br>' . date('H:i:s',strtotime($registro[Fecha])) . '</td>';
				echo '<td class="GestionesBordesLaterales">' . $registro[Nota] . '</td>';
				echo '<td>' . $registro[Usuario] . '</td>';
				echo '</tr>';
			}
		echo('</table>');
	}else{
		echo('<div style="overflow:scroll;height:100%em;width:100%">');
		echo('<center>No existen registros.</center>');
		echo('</div>');
	}
	echo('<div id="AgregarGestion">');
	echo '<input type="text" class="NuevaGestion" name="txtGestion" id="txtGestion">';
	echo '<input type="button" class="BotonEnviarGestion" onClick="RegistrarGestion(\'' . $numorden . '\',\'A\')" value="Enviar" />';
	echo '<a onClick="MarcarEjecutado()" style="text-decoration: none"><img id="Marca" src="images/ejecutado_0.png"><span id="MarcaTexto">MARCAR COMO EJECUTADO</span></a>';
	echo('</div>');
	
	//CODIGO COMENTADO LISTO PARA HABILITARLO COMENTADO 09-11-2018
	$detalles = $x->tipoRegistroHoraAsistencias($numorden);
	if (count($detalles)>0){
	    echo '<br><h><font color=\'blue\'>REGISTRO DE TIEMPO DE TRABAJO</font></h>';
	    echo '<div style="overflow:scroll;height:100%;width:100%">';
	    echo '<table class="Gestiones">';
	    echo '<tr><td>';
	    echo '<select id="cmbTipoRegistro" class="Combo" onChange="obtenerPosicion1()">';
	    echo '<option>---</option>';
	    foreach($detalles as $detalle){
	        echo '<option value=\''. $detalle[idTipo] . '\'>';
	           echo $detalle[descripcion];
	        echo '</option>';
	    }
	    echo '</select></td>';
	    echo '<td><input type="button" id="enviar" value="Registrar.." onClick="insertaRegistroHoras(\''. $numorden .'\',\'' . $_SESSION['log_USUARIO'] . '\')"></td></tr></table></div>';
	}
	
?> 	
</body>
</html>