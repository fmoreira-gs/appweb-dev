<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	//CARGAR ZONAS
	$sqlzonasmant = $x->sectorCliente();
	
	//OBTENER PASO DE DATO (GET)
	$ZonaR = $_GET['zona'];
	if(!isset($ZonaR)&&$_SESSION['log_IDTECNICO']!='-1'){
		$ZonaR = $_SESSION['log_IDZONA'];
	}
?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?php echo(Config::$mvc_titulo); ?></title>
<!--  ESTILOS  -->
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
</head>
<body>
<?php
    //FORMULARIO DE ZONAS PARA CONSULTA
    foreach($sqlzonasmant as $zona){
        if(isset($ZonaR) && ($ZonaR==$zona[Id_Sector]) ){
            $ZMantenimiento = ' - ZONA ' . $zona[Sector];
        }
    }
    echo('<div id="Titulo">CLIENTES' . $ZMantenimiento . '<br /><span id="Cuenta"></span></div>');
    echo '<br /><br /><center>';
    echo 'Zona de CLIENTE: ';
    echo ' <select name="zona" id="zona" class="Combo Extendido" onchange="clienteCoordenadas(0)">';
    echo '<option>';
    echo '----';
    echo '</option>';
    foreach($sqlzonasmant as $zona){
        echo '<option value="'. $zona[Id_Sector] . '" ';
        if(isset($ZonaR) && ($ZonaR==$zona[Id_Sector])){
            echo ('selected');
        }
        echo '>';
        echo $zona[Sector];
        echo '</option>';
    }
    echo '</select>';
    echo '</center>';

    //CONTENIDO - MANTENIMIENTOS
    if(isset($ZonaR)){
        $datos = $x->clientesSinPosicion($ZonaR);
        //if(count($datos)>0){
        echo('<center>CODIGO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NOMBRE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DIRECCION
					<br /><br />Registros encontrados: ' . count($datos) . '</center>');
            echo('<table class="VistaDatos" width="100%">');
            foreach ($datos as $nombres){
                //				echo '<tr id="tr_' . $nombres[ID] . '" class="Prioridad_' . $nombres[PRIORIDAD] . '">';
                echo '<tr><td><a onClick="w_posicion(\'' . $nombres[Codigo] . '\',0)">' . $nombres[Codigo] . '</a></td>';
                //echo '<td><div class="MasInfo" onclick="MasInfo(\'' . $nombres[ID] . '\')">' . $nombres[CLIENTE] . '</div><div class="MasInfo_detalle" onclick="MasInfo(\'' . $nombres[ID] . '\')" id="' . $nombres[ID] . '" style="display: none"><hr width="50%" />';
                //'</div></td>';
                echo '<td width="23%">' . $nombres[Nombre] . '</td>';
                echo '<td>' . $nombres[Direccion] . '</td>';
                //echo '<td><a onClick="Gestiones(\'' . $nombres[ID] . '\',\'M\')"><img src="images/gestion.png" class="iconoCelda" alt="Gestion" title="Gesti&oacute;n"></a></td>';
                echo '</tr>';
            }
            echo('</table>');
            echo '</form>';
        //}else{
        //    echo('<center>No se encontraron registros.</center>');
        //}
    }	
?>
</body>
</html>