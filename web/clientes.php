<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	$origen = $_GET['origen'];
	
	//SETEAR CARACTERES A UTF-8
	//ini_set('mssql.charset', 'UTF-8');
	
	//CLIENTES MONITOREO
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);	
?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?php echo(Config::$mvc_titulo); ?></title>
<!--  ESTILOS  -->
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" src="../js/jquery-3.4.1.js"></script>
</head>
<body>
<?php	
	echo('<div id="Titulo">CLIENTES MONITOREO</div>'); 	
?>
<br />
<br />
&nbsp;Buscar por:
<br />
&nbsp;<a href="#" class="Radio Radio_OFF" onClick="BuscarPor('Codigo')" id="optCodigo">&nbsp;C&oacute;digo</a>
&nbsp;<a href="#" class="Radio Radio_OFF" onClick="BuscarPor('Nombre')" id="optNombre">&nbsp;Nombre</a>
<?php
    if ($origen == 1){
       echo '<a href="#" class="Radio Radio_OFF" onClick="BuscarPor(\'RUC\')" id="optRUC">&nbsp;RUC</a>';
    }
?>
<br />
&nbsp;<input type="text" class="CajaBuscar" id="txtBuscarCliente" onkeyup="t_BuscarCliente(event,<?php echo $origen; ?>)"></input>&nbsp;<a onClick="BuscarCliente(<?php echo $origen; ?>)" class="Boton_Buscar icon_buscar"></a>
<center>
<div id="listaClientes"></div>
</center>
</body>
</html>