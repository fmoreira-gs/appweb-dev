<?php 
	session_start();
	$usuario=$_SESSION['log_USUARIO'];
	
   //Archivos 
   require_once __DIR__ . '/../app/Config.php';
   require_once __DIR__ . '/../app/Model.php';

   //SE OBTIENE EL LISTADO DE LOS TECNCOS
   $sqltecnico = $x->Tecnicos();
   
   //OBTENER Id de Tecnico
   $tecnicoId = $_POST['tecnicos']; 
   if(!isset($tecnicoId)&&$_SESSION['log_IDTECNICO']!='-1'){
   	$tecnicoId = $_SESSION['log_IDTECNICO'];
   }else{
   	$tecnicoId = urldecode($tecnicoId);
   }
   
 ?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?php echo(Config::$mvc_titulo); ?></title>
<!--  ESTILOS  -->
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
</head>
<body>
<?php
	echo('<div id="Titulo">ASISTENCIAS TECNICAS<br /><span id="Cuenta"></span></div>');
	echo '<br /><br /><center>';
	echo '<form method=\'POST\'name=\'form\'>';
		echo '<font color=\'blue\'>Escoja Nombre Del Tecnico:</font>';
		echo '<select name="tecnicos" id="tecnicos" class="Combo Extendido" onchange="Asistencias(0)">';
			echo '<option>';	
			echo '----';
			echo '</option>';
			foreach($sqltecnico as $tecnico){
				echo '<option value=\''. $tecnico['ID_TECNICO'] . '\'';
				if(isset($tecnicoId) && ($tecnicoId==$tecnico['ID_TECNICO']) ){
					echo ('selected');
				}
				echo '>';
					echo $tecnico['NOMBRE'];
				echo '</option>';
			}
		echo '</select>';
   	echo '</form>';
   	$codigoCliente = "";
	if(isset($tecnicoId)){
		$asistencias= $x->OrdenesAsignadas($tecnicoId);
		while($row = mssql_fetch_assoc($result)){
			$asistencias[] = $row;
		}
		if(count($asistencias)>0){
			echo('<center>ORDEN&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;CLIENTE&nbsp;&nbsp;&nbsp;|
					&nbsp;&nbsp;&nbsp;CITA&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;MONITOREO&nbsp;&nbsp;&nbsp;|
					&nbsp;&nbsp;&nbsp;HISTORIAL&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;GESTION</center>');
			echo('<center>Registros encontrados: ' . count($asistencias) . '</center>');
			echo('<table class="VistaDatos" width="100%">');
			foreach ($asistencias as $asignacion){
				$fechacita = date('d-m-Y H:i',strtotime($asignacion['Cita']));
				$fechafincita = date('d-m-Y',strtotime($asignacion['fechaFinCita']));
				echo '<td>' . $asignacion['Numero'] . '</td>';
				echo '<td><div class="MasInfo" onclick="MasInfo(\'' . $asignacion['Numero'] . '\')">' . $asignacion['Nombre'] . '</div>
					<div class="MasInfo_detalle" onclick="MasInfo(\'' . $asignacion['Numero'] . '\')" id="' . $asignacion['Numero'] . '" 
					style="display: none"><hr width="50%" /><b>Requerimiento: </b>' . $asignacion['Nota'] . '<hr width="50%" />
					<b>Direccion: </b>' . $asignacion['Direccion'] . '</div>
				</td>';
				echo '<td width="23%">' . $fechacita . '</td>';
				if($asignacion['tipo_documento']=='rma'){
					$codigoCliente = $asignacion['Monitoreo'];
				}else if($asignacion['tipo_documento']=='cotizacion'){
					$codigoCliente = $asignacion['codigo_monitoreo'];
				}else{
					$codigoCliente = $asignacion['Monitoreo'];
				}
				if($asignacion['Monitoreo']!=null || $asignacion['codigo_monitoreo']!=null){
					echo '<td width="2%"><a onClick="DatosClientes(\'' . $codigoCliente . '\')">' . $codigoCliente . '</a></td>';
					echo '<td class="noBorderRight"><a onClick="Historial(\'' . $codigoCliente . '\')" href="#"><img src="images/historial.png" style ="height: 3em; width: 3em;" alt="Historial" title="Historial"></a></td>';
				}else{echo '<td></td><td></td>';}
				echo '<td><a onClick="Gestiones(\'' . $asignacion['Numero'] . '\',\'A\')" href="#"><img src="images/gestion.png" style ="height: 3em; width: 3em;" alt="Gestion" title="Gesti&oacute;n"></a></td>';
				echo '</tr>';
			}
			echo '</tr></table>';
		}else{
			echo('<center>No se encontraron registros.</center>');
		}
	}
?>
</body>
</html>