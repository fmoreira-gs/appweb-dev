<?php
	
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	//ini_set('mssql.charset', 'UTF-8');
	
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);	
	
	session_start();
	$usuario=$_SESSION['log_USUARIO'];
	
	$codigocli = urldecode($_POST['txtCodCli']);
	$marca = urldecode($_POST['marca']);
	$modelo = urldecode($_POST['modelo']);
	$version = urldecode($_POST['version']);
	$operadora = urldecode($_POST['operadora']); 
	$nomcli = urldecode($_POST['txtNomCli']);
	$dircli = urldecode($_POST['txtDirCli']);
	$telcli = urldecode($_POST['txtTelCli']);
	$refcli = urldecode($_POST['txtRefCli']);
	$movilSms = urldecode($_POST['txtMovilSms']);
	$lineaConv = urldecode($_POST['txtLineaConv']);
	$tipoLinea = urldecode($_POST['txtTipoLinea']);
	$cambioClave = urldecode($_POST['cambioClave']);
	$txtEstadoClave = urldecode($_POST['txtEstadoclave']);
	
	if($cambioClave != $txtEstadoClave){
	    $x ->insertaRegistros('auditoriaClaves','cuenta,tipo,usuario,fecha,accion','\'' . $codigocli . '\',\'SDI\',\'' . $usuario . '\',GETDATE(),\'EDICION\'');
	}
	
	$sinLinea = urldecode($_POST['sinLinea']);
	if($sinLinea == 'true'){
		$sinLinea=1;
	}else{$sinLinea=0;}
	
	$lineaAveriada = urldecode($_POST['lineaAveriada']);
	if($lineaAveriada == 'true'){
		$lineaAveriada=1;
	}else{$lineaAveriada=0;}
	
	if(isset($codigocli)&&isset($marca)&&isset($modelo)&&isset($version)&&isset($operadora)&&isset($nomcli)&&isset($dircli)&&isset($telcli)&&isset($refcli)&&isset($movilSms)){
		$sqleditcliente ="UPDATE Cliente_Monitoreo_Adt set Nombre='$nomcli', Direccion='$dircli',Telefonos='$telcli',Referencia='$refcli',
		MarcaAlarma='$marca',ModeloAlarma='$modelo',VersionAlarma='$version', TelefonoEnvioSMS='$movilSms', ProveedorLineaConvencional='$operadora',NumeroTelefonoConvencional='$lineaConv',
		TipoLineaConvencional='$tipoLinea' , UserMod='$usuario', DateUserMod=GETDATE(), Validado=0, Linea='$sinLinea', Consigna='$lineaAveriada', nuevaClavePGM='$cambioClave' where Codigo='$codigocli'";
		$resultado = mssql_query($sqleditcliente,$x->conexion);
		if(!$resultado){
			die('<b>Error #M03:</b> Error al obtener los datos solicitados en la tabla Codigos_Pase_Adt.');
			$x->GuardaLOGS($usuario,"ERROR","Error al  actualizar datos en la base de datos SQL[" . $sqleditcliente . "].");
		}
		$x->GuardaLOGS($usuario,"EDICION","Se registro una actualizacion en la base de datos SQL[" . $sqleditcliente . "].");
	}	
?>