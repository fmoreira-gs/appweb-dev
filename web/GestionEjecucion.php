<?php
	session_start();
	$usuario=$_SESSION['log_USUARIO'];	
	
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	//ini_set('mssql.charset', 'UTF-8');

	$idmanteminimiento = $_GET['idmant'];
	$noCache = $_GET['NoCache'];
	$idReq = $_GET['idReq'];
	 
	$idmantenimientonull = preg_replace('/[^0-9]/', '',$idmanteminimiento);
	
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);
	$datos = $x->MantenimientosGestiones($idmanteminimiento, $idReq);
	$info = $x->DatosMantenimiento($idmanteminimiento);

?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
<?php 
	
	while($row = mssql_fetch_assoc($result)){
		$datos[] = $row;
	}
	
	echo '<div class="ventana_titulo">GESTIONES';
	echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%" /></a></div>';
	echo '</div>';
	
	//Cargar informacion de mantenimiento
	echo('<div class="info">&nbsp;<b>CUENTA: </b>' . $info[0][CUENTA] . ' - ' . substr($info[0][CLIENTE],0,30) . '</div><hr width="25%">');

	if(count($datos)>0){
		echo('<div class="G_Contenedor">');		
		echo('<table class="Gestiones">');
		echo('<tr>');
		echo('<th>FECHA</th>');
		echo('<th>NOTA</th>');
		echo('<th>USUARIO</th>');
		echo('<th width=30>idREQ</th>');
		echo('</tr>');
		$css = 0; $class = '';
		foreach ($datos as $gestion){
			// linea para dar formato a la variable de fecha
			$fecha = date('d-m-Y',strtotime($gestion[Fecha]));
			if($css==1){
				$class = 'class="impar"';
				$css = 0;
			}else{
				$class = '';
				$css = 1;
			}
			echo '<tr ' . $class . '>';
			echo '<td id="GestionFecha">' . $fecha . '</td>';
			echo '<td class="GestionesBordesLaterales">' . $gestion[Nota] . '</td>';
			echo '<td>' . $gestion[Usuario] . '</td>';
			echo '<td width=30><center>' . $gestion[idReq] . '</td>';
			echo '</tr>';			
		}
		echo('</table>');
		echo('</div>');
	}else{
		echo('<div class="G_Contenedor">');
		echo('<center>No existen registros.</center>');
		echo('</div>');
	}
	if ($idReq != undefined ) {
		echo('<div id="AgregarGestion">');
		echo '<input type="text" class="NuevaGestion" name="txtGestion" id="txtGestion">';
		echo '<input type="button" class="BotonEnviarGestion" onClick="RegistrarGestion(\'' . $idmanteminimiento . '\',\'M\',' . $idReq . ')" value="Enviar" />';
		echo '<a onClick="MarcarEjecutado()" style="text-decoration: none"><img id="Marca" src="images/ejecutado_0.png" style ="height: 3em; width: 3em;"><span id="MarcaTexto">MARCAR COMO EJECUTADO</span></a>';
		echo('</div>');
	}
?>
</body>
</html>