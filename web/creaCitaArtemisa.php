<?php
	//ARCHIVOS DE CONFIGURACION GLOBAL
	session_start();
    $usuario = $_SESSION['log_USUARIO'];
    $usuario = $usuario.trim();
	
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');

	
    //RECOPILACION DE VARIABLES DE PASO
    $nombre = filter_input(INPUT_POST,'nombre');
    $apellido = filter_input(INPUT_POST,'apellido');
    $direccion = filter_input(INPUT_POST,'direccion');
    $telefono = filter_input(INPUT_POST,'telefono');
    $agencia = filter_input(INPUT_POST,'agencia');
    $requerimiento = filter_input(INPUT_POST,'requerimiento');
    $contacto = $nombre . ' ' . $apellido;
    $fechaCita = filter_input(INPUT_POST,'fechaCita');
    $codCli = filter_input(INPUT_POST,'idCliente');
    $opt = filter_input(INPUT_POST,'opcion');

    if($opt==1){
        //SI SE DEBE CREAR UN NUEVO CLIENTE
        $nomAgencia = $cone->retornaAgencia($agencia);
        $nomAgencia = $nomAgencia[0]['Descripcion'];

        $nomSP = 'MFCLIENTES_GUARDAR';
        $parametros = "'$contacto','$apellido','$nombre',NULL,NULL,'$direccion',NULL,'$telefono',NULL,'$nomAgencia',NULL,NULL,NULL,'$usuario',0,'',0,NULL";
        $codCli = $art->creaClienteArtemisa($nomSP, $parametros);
    }
    
    //PARAMETROS PARA CREACION DE LA NUEVA CITA
    $nomSP1 = 'SP_CITAS_WEB';
    $parametros1 = "'$codCli','$fechaCita','$requerimiento','$usuario'";
    $idCita = $art->creaCitaArtemisa($nomSP1, $parametros1);
    
    echo json_encode($idCita);
?>