<?php
    //Archivos
    require_once __DIR__ . '/../app/Config.php';
    require_once __DIR__ . '/../app/Model.php';
    
    $codigocli = $_GET['codigo'];
    $emergente = $x-> ObtenerDatos('Telefonos_Emergentes_Adt','*','Where Codigo=\'' . $codigocli . '\'','Order by Orden');
    
    $prioridad = $x-> prioridadEmergente();
    $cargo = $x-> cargosEmergente();
    
    echo('<table class="VistaDatos1" width="100%">');
    echo('<tr>');
    echo('<th width="10%">#</th>');
    echo('<th>Nombre</th>');
    echo('<th>N&uacute;mero</th>');
    echo('<th width="10%">Prioridad</th>');
    echo('<th>Cargo</th>');
    echo('<th></th>');
    echo('</tr>');
    echo('<tr>');
    ///Contador
    $contemergente=1;
    foreach ($emergente as $personal){
        echo('<td>');
        $numerEmergente;
        echo('<select class="ComboN" id="PE' . $contemergente . '_txtorden">');
        for($i=1;$i<=Config::$MaxItem;$i++){
            echo('<option value="'. $i .'"');
            if($i==$personal[Orden]){
                echo(' selected');
                $numerEmergente = $personal[Orden];
            }
            echo('>');
            echo($i);
            echo('</option>');
        }
        echo('</select>');
        echo('</td>');
        echo('<td><input value="' . $numerEmergente . '" type="hidden" name="PE' . $contemergente . '_numOrden" id="PE' . $contemergente . '_numOrden"><input onFocus="VerMas(\'PE' . $contemergente . '_txtnombre\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="PE' . $contemergente . '_txtnombre" name="txtnombre" value="' . $personal[Nombre] . '"></td>');
        echo('<td><input onFocus="VerMas(\'PE' . $contemergente . '_txtnumero\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="PE' . $contemergente . '_txtnumero" name="txtnumero" value="' . $personal[Numero] . '"</td>');
        //echo('<td><input onFocus="VerMas(\'PE' . $contemergente . '_txtnota\')" onBlur="VerMas(\'none\')" class="Edicion" type="text" id="PE' . $contemergente . '_txtnota" name="txtnota" value="' . $personal[Nota] . '"</td>');
        echo '<td><select class="ComboPri" name="prioridad" id="PE' . $contemergente . '_prioridad">';
        echo '<option value=0> </option>';
        foreach($prioridad as $_prioridad){
            echo '<option value="'. $_prioridad[idPrioridad] . '"';
            if(isset($_prioridad[descripcion]) && ($_prioridad[idPrioridad]==$personal[idPrioridad]) ){
                echo (' selected');
            }
            echo '>';
            echo($_prioridad[descripcion]);
            echo '</option>';
        }
        echo '</select></td>';
        echo '<td><select class="Combo Extendido" name="cargo" id="PE' . $contemergente . '_cargo">';
        echo '<option value=0> </option>';
        foreach($cargo as $_cargo){
            echo '<option value="'. $_cargo[idCargo] . '"';
            if(isset($_cargo[descripcion]) && ($_cargo[idCargo]==$personal[idCargo]) ){
                echo (' selected');
            }
            echo '>';
            echo $_cargo[descripcion];
            echo '</option>';
        }
        echo '</select></td>';
        echo('<td><a class="Radio Radio_OFF" onClick="MarcarFila(\'PE' . $contemergente . '_radio\',\'PE' . $contemergente . '_numOrden\')" id="PE' . $contemergente . '_radio"></a></td>');
        echo('</tr>');
        echo('<input type=\'hidden\' name=\'txtcodigo\' value=' . $codigocli . '>');
        $contemergente++;
    }
    echo('</table>');
    echo('<br/><center>');
    if($_SESSION['log_rolUsr'] != 0){
        echo('<a class="Boton_Guardar icon_agregar" onClick="Clientes_AgregarEmergente()">&nbsp;Insertar Emergente..</a>');
    }
    echo('</center>');
    echo('<br /><br /><br /><br /><br />');
?>