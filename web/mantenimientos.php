<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	//CARGAR ZONAS
	$sqlzonasmant = $x->Zonas();
	
	//OBTENER PASO DE DATO (GET)
	$ZonaR = $_GET['zona'];
	if(!isset($ZonaR)&&$_SESSION['log_IDTECNICO']!='-1'){
		$ZonaR = $_SESSION['log_IDZONA'];
	}
?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title><?php echo(Config::$mvc_titulo); ?></title>
<!--  ESTILOS  -->
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
</head>
<body>
<?php
	//FORMULARIO DE ZONAS PARA CONSULTA
    foreach($sqlzonasmant as $zona){
       if(isset($ZonaR) && ($ZonaR==$zona['ID_ZONA]') ){
	      $ZMantenimiento = ' - ZONA ' . $zona['ZONA'];
	   }
    }
	echo('<div id="Titulo">MANTENIMIENTOS' . $ZMantenimiento . '<br /><span id="Cuenta"></span></div>'); 
	echo '<br /><br /><center>';
	echo 'Zona de Mantenimiento: ';
	echo ' <select name="zona" id="zona" class="Combo Extendido" onchange="Mantenimientos(0)">';
	echo '<option>';
	echo '----';
	echo '</option>';
	foreach($sqlzonasmant as $zona){
		echo '<option value="'. $zona['ID_ZONA'] . '" ';
		if(isset($ZonaR) && ($ZonaR==$zona['ID_ZONA]') ){
			echo ('selected');
		}
		echo '>';
		echo $zona['ZONA'];
		echo '</option>';
	}
	echo '</select>';
	echo '</center>';

	//CONTENIDO - MANTENIMIENTOS
	if(isset($ZonaR)){
		$datos = $x->Mantenimientos($ZonaR);
		while($row = mssql_fetch_assoc($result)){
			$datos[] = $row;
		}
		if(count($datos)>0){
			echo('<center>CUENTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLIENTE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CITA
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HISTORIAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GESTION<br /><br />Registros encontrados: ' . count($datos) . '</center>');
			echo('<table class="VistaDatos" width="100%">');		
			foreach ($datos as $nombres){
				$detalleMan = $x -> MantenimientosDetalle($nombres['ID']);
				echo '<tr id="tr_' . $nombres[ID] . '" class="Prioridad_' . $nombres['PRIORIDAD'] . '">';
				echo '<td class="noBorderLeft"><a onClick="DatosClientes(\'' . $nombres['CUENTA'] . '\')">' . $nombres['CUENTA'] . '</a></td>';
				echo '<td><div class="MasInfo" onclick="MasInfo(\'' . $nombres['ID'] . '\')">' . $nombres['CLIENTE'] . '</div><div class="MasInfo_detalle" onclick="MasInfo(\'' . $nombres['ID'] . '\')" id="' . $nombres['ID'] . '" style="display: none"><hr width="50%" />';
					foreach ($detalleMan as $detalle){
						echo  '<a onClick="tipoGestion(\'' . $nombres['ID'] . '\',\'M\',' . $detalle['IdReq'] . ')">' . $detalle['IdReq'] . ' - ' . $detalle['requerimiento'] . '<hr width ="50%"></a>';	
					}							
					 '</div></td>';
				echo '<td width="23%">' . date('d-m-Y H:i', strtotime($nombres['Cita'])) . '</td>';
				echo '<td class="noBorderRight"><a onClick="Historial(\'' . $nombres['CUENTA'] . '\')"><img src="images/historial.png" class="iconoCelda" alt="Historial" title="Historial"></a></td>';
				echo '<td><a onClick="Gestiones(\'' . $nombres['ID'] . '\',\'M\')"><img src="images/gestion.png" class="iconoCelda" alt="Gestion" title="Gesti&oacute;n"></a></td>';
				echo '</tr>';
			}
			echo('</table>');
			echo '</form>';					
		}else{
			echo('<center>No se encontraron registros.</center>');
		}	
	}	
?>
</body>
</html>