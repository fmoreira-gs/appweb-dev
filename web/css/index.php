<?php 
	require_once __DIR__ . '/../../app/Config.php';
?>
<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>
	<?php echo(Config::$mvc_titulo); ?>
</title>
<?php echo('<link rel="stylesheet" type="text/css" href="' . Config::$mvc_estilo . '">')?>
</head>
<body>
<p>
	No est&aacute; autorizado para ver el contenido. Haga clic <a href="../index.php">aqu&iacute;</a> para dirigirse a la p&aacute;gina principal.
</p>	
</body>
</html>