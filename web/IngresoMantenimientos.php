<?php
	session_start();
	$usuario=$_SESSION['log_USUARIO'];	
	
	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	$codigo = $_GET['codigo'];
	$monitoreo = $_GET['monitoreo'];
	if($monitoreo==0){
		$datoscli = $x->ObtenerDatos('Cuentas_Monitoreo_Adt','*','Where Codigo=\'' . $codigo . '\'','');
	}else{
		$datoscli = 'XXX';
	}
	$hoy = time();
	$fecha_inicio = date("Y-m-d", $hoy);
?>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<link rel="stylesheet" type="text/css" href="css/estilo.css" />
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php 
	echo '<div class="ventana_titulo">INGRESO MANTENIMIENTOS';
	echo '<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%" /></a></div>';
	echo '</div>';
	if($datoscli != null){
		echo '<div id="G_Contenedor">';
		echo '<table class="table table-sm table-striped">';
		if ($monitoreo == 0){
			echo '<tr><td>C&oacute;digo :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="text" id="txtCodCli" value=\'' . $datoscli[0]['Codigo'] . '\' readonly></td></tr>';
			echo '<tr><td>Nombre :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="text" id="txtNomCli" value=\'' . $datoscli[0]['Nombre'] . '\' readonly></td></tr>';
			$srv = $x-> MantenimientoServicios(0);
		}else{
			echo '<input type="hidden" id="txtCodCli">';
			echo '<input type="hidden" id="txtContacto">';
			echo '<tr><td>Nombre :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="text" id="txtNomCli" placeholder="Ingrese Nombre"></td></tr>';
			echo '<tr><td>Apellido :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="text" id="txtApeCli" placeholder="Ingrese Apellido"></td></tr>';
			echo '<tr><td>Direccion :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="text" id="txtDirCli" placeholder="Ingrese Dirección"></td></tr>';
			echo '<tr><td>Ruc/Ced.:</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="number" id="txtRucCli" max="10" placeholder="Ingrese # Cédula"></td></tr>';
			echo '<tr><td>Telefono :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="number" id="txtTelCli" placeholder="Ingrese Teléfono"></td></tr>';
			echo '<tr><td>Fecha Visita :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="date" id="txtFechCita" min="' . $fecha_inicio . '" value= "' . date("Y-m-d", $hoy) . '"></td></tr>';
			echo '<tr><td>Agencia :</td>';
			echo '<td><select style="background-color:transparent; font-size:14" class="Combo Extendido" id="cmbAgenciaCli" >';
			$agencias = $cone->agencias();
			foreach($agencias as $agencia){
				echo '<option value="'. $agencia['Tipo_Cliente'] .'">';
					echo $agencia['Descripcion'];
				echo '</option>';
			}
			echo '</td></tr>';
			$srv = $x-> MantenimientoServicios(1);
		}
		if($monitoreo==0){
			$servicioSeleccionado;
			echo '<tr><td>Servicio :</td>';
			echo'<td><div id="_servicio">';
			echo '<select class="Combo Extendido form-control" name="servicios" id="servicios" onchange="CargarSubServicios()" onload="CargarSubCategoria(' . $servicioSeleccionado .')">';
			foreach($srv as $serv){
				echo '<option value="'. $serv['IdServicio'] .'"';
				if(isset($serv['IdServicio']) && ($serv['IdServicio']== -1) ){
					echo (' selected');
					$servicioSeleccionado = $serv['IdServicio'];
				}
				echo '>';
				echo $serv['Servicio'];
				echo '</option>';
			}
			echo '</select></div></td></tr>';
			echo '<input type="hidden" id="servicioSeleccionado" value =' . $servicioSeleccionado .'>';
			if($monitoreo == 0){
				$subservicios = $x-> MantenimientoSubServicios($servicioSeleccionado);
			}else{
				$subservicios = $x-> MantenimientoSubServicios(32);
			}
			$subservicioSeleccionado;
			echo '<tr><td>SubServicio :</td>';
			echo'<td><div id="_subservicio">';
			if($monitoreo == 0){
				echo '<select class="Combo Extendido form-control" name="subservicios" id="subservicios" onchange="CargarSubCategoria(' . $servicioSeleccionado .')" onClick="CargarSubCategoria(' . $servicioSeleccionado .')">';
			}else{
				echo '<select class="Combo Extendido form-control" name="subservicios" id="subservicios" ">';
			}
			foreach($subservicios as $sub){
				echo '<option value="'. $sub['IdSubServicio'] .'"';
				if(isset($sub['IdSubServicio']) && ($sub['IdSubServicio']== -101) ){
					echo (' selected');
					$subservicioSeleccionado = $sub['IdSubServicio'];
				}
				echo '>';
				echo $sub['SubServicio'];
				echo '</option>';
			}
		}
		echo '</select></div></td></tr>';
		if($monitoreo == 0){
			$subcategoria = $x-> MantenimientoSubCategoria($subservicioSeleccionado);
			if($subservicioSeleccionado != 32){
				echo '<tr><td>SubCategoria :</td>';
				echo'<td><div id="_subcategoria">';
				echo '<select class="ComboN Extendido form-control" name="subcategorias" id="subcategorias">';
				foreach($subcategoria as $cat){
					echo '<option value="'. $cat['idSubCategoria'] .'">';
					echo $cat['SubCategoria'];
					echo '</option>';
				}
				echo '</select></div></td></tr>';
			}
			echo '<input type="hidden" id="subservicioSeleccionado" value =' . $subservicioSeleccionado .'>';
		}
		echo '<tr><td>Requerimiento:</td>';
		echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="textarea" id="txtRequerimiento" placeholder="Ingrese Solicitud del Cliente" ></td></tr>';
		if($monitoreo==0){
			echo '<tr><td>Contacto :</td>';
			echo '<td><input style="background-color:transparent; font-size:14" class="form-control form-control-sm text-left  border-1" type="text" id="txtContacto" ></td></tr>';
		}
		echo '</table>';
		echo('<center>');
		if($monitoreo==0){
			echo('<a class="Boton_Guardar icon_guardar" onClick="grabaRequerimiento(' . $monitoreo . ')">&nbsp;Grabar Requerimiento</a>');
		}else{
			echo('<button class="icon_guardar btn btn-primary" onClick="compruebaExistente(' . $monitoreo . ')">&nbsp;Grabar Requerimiento</button>');
		}
		echo('</center></div>');
	}else{
		echo '<center><h3>NO SE HA ENCONTRADO CLIENTE CON EL DATO INGRESADO !!<h3>';
		echo('<br/>');
		echo('<a class="Boton_Guardar icon_cancelar" onClick="CerrarGestiones()">&nbsp;Regresar</a>');
		echo('</center>');
	}
?>
</body>
</html>