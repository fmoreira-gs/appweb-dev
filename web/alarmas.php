<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);

	//CARGAR DATOS DE ACUERDO A PARAMETROS
	$opt = $_GET['opt'];
	$var = $_GET['var'];
	$srv = $_GET['servicio'];
	$seleccionado = $_GET['seleccionado'];
	
	if(isset($opt)&&isset($var)){
		if($opt=='marca'){
			//Buscar Modelos
			$modelo = $x->ObtenerDatos('AlarmasModelo','*','where idMarca = ' . $var,'order by Modelo Asc');
			echo '<select class="Combo Extendido" name="modelo" id="modelo" onchange="CargarVersiones()">';
			foreach($modelo as $model){
				echo '<option value=' . $model[IdModelo] . '>' . $model[Modelo] . '</option>';
			}
			echo '</select>';

		}elseif($opt=='modelo'){
			$version = $x->ObtenerDatos('AlarmasVersion','*','where idModelo = ' . $var,'order by Version Asc');
			echo '<select class="Combo Extendido" name="version" id="version">';
			foreach($version as $vers){
				echo '<option value=' . $vers[IdVersion] . '>' . $vers[Version] . '</option>';
			}
			echo '</select>';
			
		}elseif($opt=='subservicio'){
			$subservicios = $x->MantenimientoSubServicios($var);
				echo '<select class="Combo Extendido" name="subservicios" id="subservicios" onchange="CargarSubCategoria(' . $var . ')" onClick="CargarSubCategoria(' . $var .')">';
				foreach($subservicios as $sub){
					echo '<option value=' . $sub[IdSubServicio] . '>' . $sub[SubServicio] . '</option>';
				}
				echo '</select>';
							
		}elseif($opt=='subcategoria'){
			$subcategoria = $x->MantenimientoSubCategoria($var);
			if ($srv != 32){
				if ($srv == -1){
					echo '<select class="ComboN Extendido" name="subcategorias" id="subcategorias">';
					foreach($subcategoria as $cat){
						echo '<option value=' . $cat[idSubCategoria] . '>' . $cat[SubCategoria] . '</option>';
					}
				}elseif ($var == -110){
					echo '<select class="ComboN Extendido" name="subcategorias" id="subcategorias">';
					foreach($subcategoria as $cat){
						echo '<option value=' . $cat[idSubCategoria] . '>' . $cat[SubCategoria] . '</option>';
					}
					echo '</select>';
				}
			}
		}
	}
?>