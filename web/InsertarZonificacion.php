<?php
	session_start();
	$usuario=$_SESSION['log_USUARIO'];

	require_once __DIR__ . '/../app/Config.php';
	require_once __DIR__ . '/../app/Model.php';
	
	//ini_set('mssql.charset', 'UTF-8');

	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);
	
	//VARIABLES GET/POST
	$codigocli = $_GET['codigo'];
	$descripcion = $_POST['txtDescripcion'];
	$zona = $_POST['txtZona'];
	
	//VENTANA
	echo('<div class="ventana_titulo">AGREGAR ZONA</div>');
	
	//LISTA DE NUMEROS
	$zonasClientes = $x->ObtenerDatosSP('SP_ZONASCLIENTES','\'' . $codigocli . '\',' . Config::$MaxItem);
	echo('<table class="VistaDatos" width="100%">');
	echo('<tr>');
	echo('<td width="8%"><b>#</b></td>');
	echo('<td>');
	echo('<select class="ComboN" name="txtZona" id="txtZona">');
	foreach ($zonasClientes as $_zonas){
		echo('<option value="' . $_zonas[NUM] . '">');
		echo($_zonas[NUM]);
		echo('</option>');
	}
	echo('</select>');
	echo('</td>');
	echo('</tr>');
	echo('<tr>');
	echo('<td><b>Descripci&oacute;n</b></td><td><input class="Edicion" type="text" name="txtDescripcion" id="txtDescripcion"></td>');
	echo('</tr>');
	echo('</table>');
	echo('<br />');
	echo('<center>');
	//VERIFICAR NUMERO
	if(isset($zona)&&isset($descripcion)){
		$reg = $x->NRegistros('Zonas_Clientes_Adt','*','where Cuenta = \'' . $codigocli . '\' and Zona = \'' . urldecode($zona) . '\'');
		if($reg>0){
			echo('<span style="color: #FF0000">Error, el n&uacute;mero de zona ingresado ya existe.</span>');
			echo('<br />');
			echo('<br />');
		}else{
			//ALMACENAR DATO
			$sqlinsertzona ='insert into Zonas_Clientes_Adt(Cuenta,Zona,Descripcion,Id_Zona) Values(\'' . $codigocli .'\',\'' . $zona . '\',\'' . $descripcion . '\',\'' . 'MSto' . $codigocli . $zona . '\')';
			mssql_query($sqlinsertzona,$x->conexion) or die('<b>Error #M03:</b> Error al obtener los datos solicitados en la tabla Zonas_Clientes_Adt.');
			$sqlinsertzona1 ='UPDATE Cliente_Monitoreo_Adt set UserMod=\'' . $usuario . '\', DateUserMod=GETDATE(), Validado=0 where Codigo=\'' . $codigocli . '\'';
			mssql_query($sqlinsertzona1,$x->conexion) or die('<b>Error #M03:</b> Error al obtener los datos solicitados. en la tabla Cliente_Monitoreo');
			echo('<span style="color: #00FF00">Correcto!, Se cre&oacute; el usuario.</span>');
			echo('<br />');
			echo('<br />');
		}
	}
	//FIN
	echo('<a class="Boton_Guardar icon_guardar" href="#" onClick="Clientes_GuardarZonificacion()">&nbsp;Guardar cambios..</a>');
	echo('&nbsp;&nbsp;&nbsp;&nbsp;');
	echo('<a class="Boton_Cancelar icon_cancelar" href="#" onClick="CerrarEmergente(\'W_Zonificacion\')">&nbsp;Volver</a>');
	echo('</center>');
?>