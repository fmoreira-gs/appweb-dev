<?php
    session_start();
    $usuario = $_SESSION['log_USUARIO'];
    
    require_once __DIR__ . '/../app/Config.php';
    require_once __DIR__ . '/../app/Model.php';
    
    //VARIABLES GET/POST
    $codigocli = $_GET['codigo'];
    $nombre = $_POST['txtnombre'];
    $orden = $_POST['txtorden'];
    $numero = $_POST['txtnumero'];
    //$nota = $_POST['txtnota'];
    $idPrioridad = $_POST['prioridad'];
    $idCargo = $_POST['cargo'];
    
    //VENTANA
    echo('<div class="ventana_titulo">AGREGAR PERSONAL EMERGENTE</div>');
    
    //LISTA DE NUMEROS
    $telefonosEmergentes = $x->ObtenerDatosSP('SP_TELEFONOSEMERGENTES','\'' . $codigocli . '\',' . Config::$MaxItem);
    $prioridad = $x -> prioridadEmergente();
    $cargo = $x-> cargosEmergente();
    
    echo('<table class="VistaDatos" width="100%">');
    echo('<tr>');
    echo('<td width="8%"><b>#</b></td>');
    echo('<td>');
    echo('<select class="ComboN" name="txtorden" id="txtorden">');
    foreach ($telefonosEmergentes as $_telefonos){
        echo('<option value="' . $_telefonos[NUM] . '">');
        echo($_telefonos[NUM]);
        echo('</option>');
    }
    echo('</select>');
    echo('</td>');
    echo('</tr>');
    echo('<tr>');
    echo('<td><b>Nombre</b></td><td><input class="Edicion" type="text" name="txtnombre" id="txtnombre"></td>');
    echo('</tr>');
    echo('<tr>');
    echo('<td><b>N&uacute;mero</b></td><td><input class="Edicion" type="text" name="txtnumero" id="txtnumero"></td>');
    echo('</tr>');
    echo('<tr>');
    echo('<td><b>Prioridad: </b></td>');
    echo '<td><select class="ComboPri" name="prioridad" id="prioridad">';
    foreach($prioridad as $_prioridad){
        echo '<option value="'. $_prioridad[idPrioridad] . '">' . $_prioridad[descripcion] . '</option>';
    }
    echo '</select></td>';
    echo('</tr>');
    echo('<tr>');
    echo('<td><b>Prioridad: </b></td>');
    echo '<td><select class="ComboPri" name="cargo" id="cargo">';
    foreach($cargo as $_cargo){
        echo '<option value="'. $_cargo[idCargo] . '">' . $_cargo[descripcion] . '</option>';
    }
    echo '</select></td>';
    echo('</tr>');
    echo('</table>');
    echo('<br />');
    echo('<center>');
    //VERIFICAR NUMERO
    if(isset($nombre)&&isset($orden)&&isset($numero)){
        $reg = $x->NRegistros('Telefonos_Emergentes_Adt','*','where Codigo = \'' . $codigocli . '\' and Orden = ' . urldecode($orden) . '');
        if($reg>0){
            echo('<span style="color: #FF0000">Error, el n&uacute;mero de lista ingresado ya existe.</span>');
            echo('<br />');
            echo('<br />');
        }else{
            //ALMACENAR DATO
            $x ->insertaRegistros('Telefonos_Emergentes_Adt','Codigo,Nombre,Numero,Orden,Id_Telefono,idPrioridad,idCargo','\'' . $codigocli . '\',\'' . urldecode($nombre) . '\',\'' . urldecode($numero) . '\',' . urldecode($orden) . ',\'' . 'MSto' . $codigocli . urldecode($orden) .'\',' . urldecode($idPrioridad) . ','. urldecode($idCargo) .'');
            
            $x-> EdicionRegistros('Cliente_Monitoreo_Adt',' UserMod=\'' . $usuario . '\', DateUserMod=GETDATE(), Validado=0','where codigo=\'' . $codigocli . '\'');
            
            echo('<span style="color: #00FF00">Correcto!, Se cre&oacute; el usuario.</span>');
            echo('<br />');
            echo('<br />');
        }
    }
    //FIN
    echo('<a class="Boton_Guardar icon_guardar" href="#" onClick="Clientes_GuardarEmergente()">&nbsp;Guardar cambios..</a>');
    echo('&nbsp;&nbsp;&nbsp;&nbsp;');
    echo('<a class="Boton_Cancelar icon_cancelar" href="#" onClick="CerrarEmergente(\'W_Emergente\')">&nbsp;Volver</a>');
    echo('</center>');
?>
