<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->Verificar('index.php');
	
	//SETEAR CARACTERES A UTF-8
	//ini_set('mssql.charset', 'UTF-8');
	
	//HISTORIAL
	//$x = new Model(Config::$mvc_server['PRUEBAS'], Config::$mvc_database['SCANNERCOP'], Config::$mvc_user, Config::$mvc_pass);

	//OBTENER PASO DE DATO (GET)
	$Cuenta = $_GET['cuenta'];	
	if(isset($Cuenta)){
		echo('<div class="ventana_titulo">HISTORIAL [' . $Cuenta . ']');
		echo('<div class="ventana_icono_cerrar"><a onClick="CerrarGestiones()"><img src="images/cerrar_ventana.png" style="width:100%" /></a></div>');
		echo('</div>');
		echo('<br />');
		//CARGAR HISTORIAL
		$Historial = $x->CargarHistorial($Cuenta,Config::$MaxHistorial);
		if(count($Historial)>0){
			echo('<div class="H_Contenedor">');
			echo('<table class="VistaDatos" width="100%">');
			echo('<tr>');
			echo('<th class="LineaB">L</th>');			
			echo('<th class="LineaB">P</th>');
			echo('<th class="LineaB">S</th>');
			echo('<th class="LineaB">Z</th>');
			echo('<th class="LineaB">E</th>');
			echo('<th class="LineaB">Descripcion</th>');
			echo('<th  class="LineaB" width="20%">Fecha</th>');
			echo('</tr>');
			$css = 0; $class = '';
			foreach($Historial as $datos){
				if($css==1){
					$class = 'class="impar"';
					$css = 0;
				}else{
					$class = '';
					$css = 1;
				}
				echo('<tr ' . $class . '>');
				echo('<td class="Datos_Seniales LineaR">');echo($datos[Linea]);echo('</td>');
				echo('<td class="Datos_Seniales LineaR">');echo($datos[Particion]);echo('</td>');
				echo('<td class="Datos_Seniales LineaR">');echo($datos[Senial]);echo('</td>');
				echo('<td class="Datos_Seniales LineaR">');echo($datos[Zona]);echo('</td>');
				echo('<td class="Datos_Seniales LineaR">');echo($datos[Evento]);echo('</td>');
				echo('<td class="Datos_Seniales LineaR">');echo($datos[Descripcion]);echo('</td>');
				echo('<td class="Datos_Seniales">');
				echo(date('d-m-Y',strtotime($datos[Llegada])));
				echo('<br />');
				echo(date('H:i:s',strtotime($datos[Llegada])));
				echo('</td>');
				echo('</tr>');
			}
			echo('</table>');
			echo('</div>');
		}else{
			echo('<center>No existen registros.</center>');
		}
	}
?>