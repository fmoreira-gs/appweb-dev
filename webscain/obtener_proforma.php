<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link href="css/tablas.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="contenedor">
<?php
$mes = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$servidor = array("MSSQL_SERVERSD","MSSQL_SERVERQUEVEDO");
ini_set('mssql.charset', 'UTF-8');
$num = $_GET['num'];
$srv = $_GET['server'];
$ficha = 0;
if($num==''||$srv==''){
	header('location:proformas.php?result=null');
}
switch($srv){
	case 'stodgo':
		$ruta = $servidor[0];
		break;
	case 'quevedo':
		$ruta = $servidor[1];
		break;
}
$conexion = mssql_connect($ruta,'sa','Systemadmin696');
if(!$conexion){
   echo "Error en la conexion";
}

$msdb=mssql_select_db("ScaiinExpress",$conexion);  
//Consulta Cabecera
$msquery = "select * from vs_Proformas where proforma = " . $num;  
$msresults= mssql_query($msquery);
if(mssql_num_rows($msresults)==0){
	echo "No hay registros";
}  
while ($row = mssql_fetch_array($msresults)) {  
echo "<table id='contenedor'><tr><td class='texto' width='557'><font size='+2'><u>GRUPO SCANNER</u></font><BR /><BR /><font size='-2'>Dir.: Calle Luis Cordero y Manuelita S&aacute;enz Telf. 1800 722663 / (02)3703460 / (02)3703422 / (02)3703286</font><BR /><BR />Documento no v&aacute;lido como factura<BR /><BR /><BR />PROFORMA</td><td width='143'><img src='images/logo1.png' /></td></tr></table>";
echo "<table id='wrap'><tr><td align='right' width='650px'>Proforma # <b>0000" . $row[0] . "</b></td><td width='50px'></td></tr></table>";
echo "<table id='wrap'><tr><td width='200px'><i>Emitido por: " . $row[20] . "</i></td><td width='500px'><i>Fecha emisi&oacute;n: " . date("d",strtotime($row[1])) . "/" . $mes[substr(date("m", strtotime($row[1])), 0, 2)-1] . "/" . date("Y",strtotime($row[1])) ."</i></td></tr></table>";
echo "<table id='cabecera'>";
echo "<tr>";
echo "<td class='titulo'>Cliente:</td>";
echo "<td>" . $row[4] . "</td>";
echo "<td class='titulo'>Tel&eacute;fono:</td>";
echo "<td colspan=\"4\">" . $row[7] . "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='titulo'>Nombre:</td>";
echo "<td>" . $row[5] . "</td>";
echo "<td class='titulo'>Fecha Cad:</td>";
echo "<td colspan=\"4\">" . date("d",strtotime($row[19])) . "/" . $mes[substr(date("m", strtotime($row[19])), 0, 2)-1] . "/" . date("Y",strtotime($row[19])) . "</td>";
echo "</tr>";
echo "<tr>";
echo "<td class='titulo'>Direcci&oacute;n:</td>";
echo "<td>" . $row[6] . "</td>";
echo "<td class='titulo'>Forma de Pago:</td>";
echo "<td>" . $row[11] . "</td>";
echo "<td class='titulo'>P. D&iacute;as:</td>";
echo "<td>" . $row[12] . "</td>";
echo "</tr>";
echo "</table>";
echo "<br />";
/////CUERPO
if($row[9]!=0)
	{
		$ficha=$row[9];
		$val_ficha = $row[26];
	}

echo "<table id='detalle'>";
echo "<tr><td width='50' class='titulo'>CANT</td><td width='470' class='titulo'>DESCRIPCION</td><td width='60' class='titulo'>PRECIO</td><td width='120' class='titulo' colspan='2'>TOTAL</td></tr>";
//Consulta Ficha
if($ficha!=0)
	{
		$msquery = "SELECT Promoción FROM Ficha_Técnica where número=" . $ficha;  
		$msresults= mssql_query($msquery); 
		while ($row = mssql_fetch_array($msresults)) {  
    		echo "<tr>";
    		echo "<td class='cantidad'></td>";
			echo "<td class='promo'>" . $row[0] . "</td>";
			echo "<td class='precio'></td>";
			echo "<td class='precio'></td>";
			echo "<td class='total'>" . number_format($val_ficha, 2) . "</td>";
			echo "</tr>";
		}
		$msquery = "SELECT A.Unidades, B.Nombre from [Proformas Detalle] A, Productos B where A.Número = " . $num . " AND A.Producto=B.Id_Producto AND A.ficha=1 ORDER BY  A.orden DESC ";  
		$msresults= mssql_query($msquery); 
		while ($row = mssql_fetch_array($msresults)) {  
    		echo "<tr>";
    		echo "<td class='cantidad'>" . $row[0] . "</td>";
			echo "<td class='contenido'>" . $row[1] . "</td>";
			echo "<td class='precio'></td>";
			echo "<td class='precio'></td>";
			echo "<td class='total'></td>";
			echo "</tr>";
		}
	echo "<tr>";
    		echo "<td height='25px' class='cantidad'></td>";
			echo "<td class='contenido'></td>";
			echo "<td class='precio'></td>";
			echo "<td class='precio'></td>";
			echo "<td class='total'></td>";
			echo "</tr>";
	}
//Consulta Detalle - Equipos
$msquery = "exec vs_Proformas_Detalle " . $num . ",0";  
$msresults= mssql_query($msquery); 
$row = mssql_fetch_array($msresults); 
if($row>0){
	echo "<tr><td class='cantidad'></td><td class='categoria'>Equipos</td><td class='precio'></td><td class='precio'></td><td class='total'>" . number_format($row[0], 2) . "</td></tr>";
	}
$msquery = "exec vs_Proformas_Detalle " . $num . ",1";  
$msresults= mssql_query($msquery);  
while ($row = mssql_fetch_array($msresults)) {  
    echo "<tr>";
    echo "<td class='cantidad'>" . $row[2] . "</td>";
	echo "<td class='contenido'>" . $row[4] . "</td>";
	echo "<td class='precio'>" . number_format($row[3], 2) . "</td>";
	echo "<td class='precio'>" . number_format($row[5], 2) . "</td>";
	echo "<td class='total'></td>";
	echo "</tr>";
}
//Consulta Detalle - Materiales
$msquery = "exec vs_Proformas_Detalle " . $num . ",2";  
$msresults= mssql_query($msquery);  
$row = mssql_fetch_array($msresults); 
if($row>0){
	echo "<tr><td class='cantidad'></td><td class='categoria'>Materiales</td><td class='precio'></td><td class='precio'></td><td class='total'>" . number_format($row[0], 2) . "</td></tr>";
}
//Consulta Detalle - Servicios
$msquery = "exec vs_Proformas_Detalle " . $num . ",3";  
$msresults= mssql_query($msquery);  
$row = mssql_fetch_array($msresults); 
if($row>0){
	echo "<tr><td class='cantidad'></td><td class='categoria'>Servicios</td><td class='precio'></td><td class='precio'></td><td class='total'>" . number_format($row[0], 2) . "</td></tr>";
}
echo "<tr><td height='200px' class='cantidad'></td>";
			echo "<td class='contenido'></td>";
			echo "<td class='precio'></td>";
			echo "<td class='precio'></td>";
			echo "<td class='total'></td>";
			echo "</tr>";
echo "</table>";
//////PIE DE PAGINA
$msquery = "select * from vs_Proformas where proforma = " . $num;  
$msresults= mssql_query($msquery);  
$row = mssql_fetch_array($msresults);
echo "<table id='wrap' border=\"0\">";
echo "<tr>";
echo "<td colspan=\"2\" width=\"550\"><b>Promoci&oacute;n v&aacute;lida &uacute;nicamente con contrato del servicio de monitoreo por 1 a&ntilde;o</b></td>";
echo "<td width=\"50\"><b>Suman:</b></td>";
echo "<td width=\"100\" align=\"right\"><b>" . $row[17] . "</b></td>";
echo "</tr>";
echo "<tr>";
echo "<td colspan=\"2\" rowspan=\"2\" width=\"600\" valign=\"top\"><b><i>Observaciones: </b></i>" . $row[15] . "</td>";
echo "<td width=\"50\"><b>Descuento:</b></td>";
echo "<td width=\"100\" align=\"right\"><b>0.00</b></td>";
echo "</tr>";
echo "<tr>";
echo "<td width=\"50\"><b>I.V.A.:</b></td>";
echo "<td width=\"100\" align=\"right\"><b>" . $row[13] . "</b></td>";
echo "</tr>";
echo "<tr>";
echo "<td width=\"150\"><b>Garant&iacute;a</b></td>";
echo "<td width=\"450\">Un a&ntilde;o en todos los equipos. Contra defectos de fabricaci&oacute;n.</td>";
echo "<td width=\"50\"><b><i>TOTAL</i></b></td>";
echo "<td style=\"border: thin; border-top-style: solid;\" width=\"100\" align=\"right\"><b>" . $row[16] . "</b></td>";
echo "</tr>";
echo "<tr>";
echo "<td width=\"150\"><b>Tiempo de Entrega</b></td>";
echo "<td width=\"450\">Ocho d&iacute;as laborables.</td>";
echo "</tr>";
echo "<tr>";
echo "<td width=\"150\"><b>Descuento</b></td>";
echo "<td width=\"450\">Aplica solo a equipos adicionales.</td>";
echo "<td colspan=\"2\" align=\"center\"><i>(Estos valores no incluyen trabajos de obra civil)</i></td>";
echo "</tr>";
echo "<tr>";
echo "<td colspan=\"4\" height=\"40\"></td>";
echo "</tr>";
echo "</table>";
echo "<table id=\"wrap\">";
echo "<tr>";
echo "<td style=\"border: thin; border-top-style: solid;\" align=\"center\" width=\"250\" contenteditable=\"true\">Asesor Comercial</td>";
echo "<td width=\"450\"></td>";
echo "</tr>";
echo "</table>";
}
?>
</div>
</body>
</html>
