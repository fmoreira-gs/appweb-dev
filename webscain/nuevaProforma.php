<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

    $local = $_SESSION['log_LOCAL'];
    //VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
    $ver->VerificarSCAIIN('index.php',$local);

    $busqueda = $_GET['busqueda'];
    $num = $_GET['num'];
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
      <!-- Bootstrap core CSS -->    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/select2.css">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="js/script.js"></script> 
    <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../js/select2.js"></script>
  </head>
  <body>
    <?php
      echo('
        <form>
          <input type="radio" id="R_nombre" class="form-control-inline" name="parametro" value="Nombre" style="width: 5%; height:5%;" checked />Nombre
          <input type="radio" id="R_rucced" class="form-control-inline" name="parametro" value="RUC" style="width: 5%; height:5%;" />Cedula/RUC
          <input type="radio" id="R_codigo" class="form-control-inline" name="parametro" value="Codigo" style="width: 5%; height:5%;" />C&oacute;digo
          <input type="hidden" id="txtNumPof" value=\'' . $num . '\'>
          ');
          if($busqueda != 'proforma'){
            echo '<input type="radio" id="R_monitoreo" class="form-control-inline" name="parametro" value="Monitoreo" style="width: 5%; height:5%;" />Monitoreo';
          }
          echo ('<br><br><input type="text" class="textbuscar" name="buscarCli" id="buscarCli" />
          <button type="button" class="btn btn-success" id="btnBuscar" onClick=BusquedaCliente(\'' . $busqueda . '\')><i class="fa fa-wpexplorer"></i> Buscar</button> 
          <div>
            <br><center>
              <button type="button" class="btn btn-primary" onClick=cierraVentana()><i class="fa fa-reply"></i> Cancelar</button>');
              if($busqueda == 'total' || $busqueda == 'cliente'){
                  echo '<button type="button" class="btn btn-sm btn-warning" onClick=insertaNuevoCliente(\'' . $busqueda . '\')><i class="fa fa-file"></i> Cliente Nuevo</button>'; 
              }
              echo('</center>
          </div>
          <center>
          <div id="buscaparametro">
            <!-- // aqui el resultado de la consulta  -->
          </div>
          </center>		
          <!-- <center><br><a href="#" onclick="GuiaCobro()" class="vinculo icon_cancelar" style="font-family: italic;" > Atras</a></center>  -->
          </div>
          
        </form>
      ');
    ?>
  </body>
</html>