<?php
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$local = $_SESSION['log_LOCAL'];
	
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->VerificarSCAIIN('index.php',$local);
	$usuario = $_SESSION['log_IDUSUARIO'];
	$sistema= $_SESSION['log_SISTEMA'];
	
	$permiso = $cone->permisoguia($usuario);
	$permiso = $permiso[0][dato];
	$UrsArtemisa = $art -> usuarioArtemisa($_SESSION['log_IDEMPLEADO']);
	
	$hoy= date('d') . '-' . date('m'). '-' . date('Y');
	$hoy = date('Y-m-d',strtotime($hoy));
?>	
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-type" content="initial-scale=1.0; user-scalable=yes; charset=utf-8" />
		<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
		<!-- Bootstrap core CSS -->    
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css"href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
  <body>
  <?php
	//ARCHIVO PARA LA GENERACION DE MENU	
	echo('<div id="barraSuperior">');
	echo('<a href="index.php" id="logoSmall"></a>');
	echo('<!-- MENU PRINCIPAL -->');
	echo('<ul class="menu">');
		echo('<li>');
		echo('<a class="desplegable icon_menu principal"> MENU PRINCIPAL</a>');
		//SUBMENU
			echo('<ul class="submenu">');
			/*echo('<li>');
			echo('<hr />');
			echo('</li>');*/
			echo('<hr />');
			//------- break
			if($_SESSION['log_SISTEMA']=="SCAIIN" && $permiso==1){	
				echo('<li>');
				echo('<a onClick="GuiaCobro()"><i class="fa fa-money"></i> GUIA DE COBRO </a><hr>');
				echo('</li>');
				//------- break
				echo('<li>');
				echo('<a onClick="proformas()"><i class="fa fa-tasks"></i> PROFORMAS</a><hr>');
				echo('</li>');
				//------- break
				//------- break
				echo('<li>');
				echo('<a onClick="ConsultaClientes()" ><i class="fa fa-users"></i> CONSULTA CLIENTES</a><hr>');
				echo('</li>');
				//------- break
				echo('<li>');
				echo('<a onClick="salidaClientes()" ><i class="fa fa-user-times"></i> SALIDA CLIENTES</a><hr>');
				echo('</li>');
				//	------- break
				echo('<li>');
				echo('<a onClick="ConsultaProductos(\'consulta\')" ><i class="fa fa-cubes"></i> CONSULTA PRODUCTOS</a><hr>');
				echo('</li>');

				if(count($UrsArtemisa)>0){
				    //------- break
				    echo('<li>');
				    echo('<a onClick="ingresoGestionCitas(\'' . $UrsArtemisa[0][DCEUSU_IDUSUA] . '\',\''. $hoy .'\', \''. $hoy .'\')" ><i class="fa fa-tty"></i> INGRESAR GESTION CITAS</a><hr>');
				    echo('</li>');
				    //------- break
				}
			}	
			echo('<li>');
			echo('<a href="logout/index.php"><i class="fa fa-sign-out"></i> CERRAR SESION</a>');
			echo('</li>');
			//------- break			 		
		echo('</ul>');
		//-- FIN SUBMENU
		echo('</li>');
	echo('</ul>');
	echo('<!-- FIN MENU -->');
	echo('</div>');	
?>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script> 
	<script type="text/javascript" src="../js/select2.js"></script>
  </body>
</html>