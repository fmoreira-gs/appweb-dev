<?php 

	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$local = $_SESSION['log_LOCAL'];
	
	$guia= $_GET['guia'];
	$banco=$_GET['banco'];
	$caja=$_GET['caja'];
	$factura=$_GET['factura'];
	$cuota=$_GET['cuota'];
	$cheque=$_GET['cheque'];
	$valor=$_GET['valor'];
	$postfechado=$_GET['postfechado'];
	$retencion=$_GET['retencion'];
	$cuenta=$_GET['cuenta'];
	$fechapos=$_GET['fechapos'];
	$tipo=$_GET['tipo'];
	$denominacion = $_GET['denominacion'];
	$cantidad = $_GET['cantidad'];
	$numero_orden = $_GET['numero'];
	
	$transf=$_GET['transf'];
	$tarjeta =$_GET['tarjeta'];
	$autorizacion =$_GET['autorizacion'];
	$numtarjeta =$_GET['numtarjeta'];
	$idTarjeta =$_GET['idtarjeta'];
	$fechatr =$_GET['fechatr'];
	$codImpuesto =$_GET['codImp'];
	$detImpuesto =$_GET['detImp'];
	$idRubro =$_GET['idRubro'];
	
	if($retencion== null){
		$retencion = 'NULL';
	}
	if($fechapos!= null){
		$fechapos= date("Y-m-d H:i:s",strtotime($fechapos));
	}
	if($fechapos==''){
		$fechapos='NULL';
	}
	switch ($tipo) {
		case 'cheque':
		    $cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$cheque,$valor,$postfechado,$retencion,$cuenta,$fechapos,$denominacion,$numero_orden,$local);
		break;
		
		case 'efectivo':
		    $cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$cheque,$valor,$postfechado,$retencion,$cuenta,$fechapos,$denominacion,$numero_orden,$local);
		break;
		
		case 'billetes':
			$valor=$cantidad;
			$cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$cheque,$valor,$postfechado,$retencion,$cuenta,$fechapos,$denominacion,$numero_orden,$local);
		break;
			
		case 'monedas':
			$valor=$cantidad;
			$cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$cheque,$valor,$postfechado,$retencion,$cuenta,$fechapos,$denominacion,$numero_orden,$local);
		break;
		case 'transferencia':
		    $cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$transf,$valor,$postfechado,$retencion,$fechatr,$fechapos,$denominacion,$numero_orden,$local);
		break;
		case 'tarjeta':
		    $cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$tarjeta,$valor,$autorizacion,$retencion,$numtarjeta,$idTarjeta,$denominacion,$numero_orden,$local);
		break;
		case 'retencion':
		    $cone ->	EditarRegistro($tipo,$guia,$banco,$caja,$factura,$cuota,$codImpuesto,$valor,$detImpuesto,$retencion,$idRubro,$idTarjeta,$denominacion,$numero_orden,$local);
		break;
	}

	header('Location:index.php');

?>