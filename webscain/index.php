<?php 

	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

?>	
<html>
	<head>
		<meta http-equiv="Content-type" content="initial-scale=1.0; user-scalable=yes; charset=utf-8" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
    	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css" href="../css/select2-bootstrap.css">
		<link rel="stylesheet" type="text/css"href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<script type="text/javascript" src="js/script.js"></script> 
		<script type="text/javascript" src="../js/jquery-3.4.1.js"></script>
		<script type="text/javascript" src="../js/select2.js"></script>
	</head>
	<?php 
		$local =  $_POST['sucursal'];
		session_start();
		if(!isset($_SESSION['log_USUARIO'])){
			echo '<body class="FondoAzul">';
			echo '<center><a href="../index.php"><div id="logo"></div></a><br/>';			
			echo '<font color="Blue">Escoja la Agencia:</font>';
			echo '<form name="autenticar" method="post">';
				$sucursal = $SD->agencias();
				echo '<select name="sucursal" id="sucursal" class="Combo" onchange=submit(); required>';
				echo '<option></option>';
				foreach($sucursal as $agencia){
					echo '<option value="' . $agencia[Tipo_Cliente] . '">';
					echo $agencia[Descripcion];
					echo '</option>';
				}
				echo '</select>';
				echo '</form></center>';
				
				if($local!= null){
				$ver = new Login();
				$ver->VerificarSCAIIN('index.php',$local);
				//VERIFICAR SI ESTA AUTENTICADO
			}

		}else{
			//include_once ('js/script.js');
			//CARGA DE SESIONES
				//session_start();
				$usuario = $_SESSION['log_IDUSUARIO'];
				$permiso = $cone->permisoguia($usuario);
				$permiso = $permiso[0][dato];
				
			//CARGA DE MENU PRINCIPAL 
				require 'menu.php';
			//--FIN MENU
			
			echo('<div id="contenido" class="contenidoIndex">');
			
			//CONTENIDO
			echo('<div id="AjaxContent">');
			echo('&nbsp;&nbsp;Bienvenido, ');
			echo('<br />&nbsp;&nbsp;<b>' . $_SESSION['log_EMPLEADO'] . '</b>');
			echo('<br />&nbsp;&nbsp;Inicio sesion en: ' . $_SESSION['log_TIEMPOSESION'] . '<br />');
			if($_SESSION['log_SISTEMA']=="SIAC" || $permiso!=1){
				echo('<br /><hr width="50%" />Al momento las opciones se encuentran habilitadas &uacute;nicamente para personas autorizadas a realizar Guias de Cobro. Gracias por su comprensi&oacute;n.');
			}
			//--FIN CONTENIDO
			echo('<br />');
			//echo('<a href="#" onClick="obtenerPosicion(\'PosicionGPS\')">Cargar</a>&nbsp;<a id="PosicionGPS" style="display: none" href="#">XXXXXXXXXXXXXXXXx</a>');
			echo('</div>');
			echo('</div>');
			//VENTANA EMERGENTE
			echo('<div id="facturasPendientes" class="ventana" title="Facturas" style="display: none"></div>');
			echo('<div id="Block" style="display: none"></div>');
		}
	?>	
</body>
</html>
