<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	//OBTENER PASO DE DATO (GET)
	$opcion = $_GET['opcion'];	
	$criterio = $_GET['criterio'];	
	$local = $_SESSION['log_LOCAL'];
	$tipo = $_GET['tipo'];
	$numProforma = $_GET['numProforma'];

	if(!isset($_SESSION['log_USUARIO'])){
	    header('location:/final/index.php');
	}	

?>

<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title><?php echo(Config::$mvc_titulo); ?></title>
	<link rel="stylesheet" type="text/css" href="css/estilos.css" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/select2.css">
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<?php
		if(isset($opcion)){
			$Productos = $cone->BuscarProductos($opcion,$criterio,$local, $tipo);
			if(count($Productos)>0){
				echo('<br>
				<div id="cheque">
					<div id="datosCliente" class="table-responsive">
						<table class="table table-sm table-striped">
						<thead class="thead-dark">
							<tr class="active">
								<th>Codigo</th>
								<th width="50%">Producto</th>');
								if($tipo=='produccion'){
									echo '<th>Total</th>';
								}else{
									echo '<th>Stock</th>';
									echo '<th>Precio A</th>';
									echo '<th>Precio B</th>';
								}
						echo ('</tr>
						</thead>
						<tbody>');
						$cont=1;
						foreach ($Productos as $datos) {
							switch ($tipo) {
								case 'consulta':
									echo('<tr>');
									echo('<td style="font-size: 0.8em"> ' . $datos['Id_Producto'] . '</td>');
									echo('<td>' . $datos['Nombre'] . '</td>');
									echo('<td align="center"> ' . $datos['Existencias'] . '</td>');
									echo('<td align="right">' . number_format($datos['Precio_A'],2) . '</td>');
									echo('<td align="right">' . number_format($datos['Precio_B'],2) . '</td>');
									echo('</tr>');
									break;
								case 'servicio':
									echo('<tr>');
									echo('<td><input  class="form-control form-control-sm text-left  border-0" type="text" value="' . $datos['Id_Producto'] . '"  id="txtIdProducto' . $cont . '" readonly/></td>');
									echo('<td width="25%"><input  class="form-control form-control-sm text-left  border-0" type="text" value="' . $datos['Nombre'] . '"  id="txtNomProducto' . $cont . '" readonly/></td>');
									echo('<td><input class="form-control form-control-sm text-center  border-0" type="text" value="' . $datos['Existencias'] . '"  id="txtExistencias' . $cont . '" readonly/></td>');
									echo('<td><input class="form-control form-control-sm text-right  border-0" type="text" value="' . number_format($datos['Precio_A'],2) . '"  id="txtPrecioA' . $cont . '" readonly/></td>');
									echo('<td><input class="form-control form-control-sm text-right  border-0" type="text" value="' . number_format($datos['Precio_B'],2) . '"  id="txtPrecioB' . $cont . '" readonly/></td>');
									echo('<td><input type="hidden" value="' . $datos['UnidadxEntera'] . '"  id="txtUnidxEntera' . $cont . '" /></td>');
									echo('<td width="10%"><button type="button" class="btn btn-success" onclick=editaCambiosProforma(' . $numProforma . ',"' . $tipo . '",' . $cont . ')><i class="fa fa-plus"></i></button></td>');
									echo('</tr>');
									break;
								case 'produccion':
									echo('<tr>');
									echo('<td><input  class="form-control form-control-sm text-left  border-0" type="text" value="' . $datos['Id_Producto'] . '"  id="txtIdProductoPrd' . $cont . '" readonly/></td>');
									echo('<td width="25%"><input  class="form-control form-control-sm text-left  border-0" type="text" value="' . $datos['Nombre'] . '"  id="txtNomProductoPrd' . $cont . '" readonly/></td>');
									echo('<td><input class="form-control form-control-sm text-right  border-0" type="text" value="' . number_format($datos['BaseImp'],2) . '"  id="txtPrecioPrd' . $cont . '" readonly/></td>');
									echo('<td><input type="hidden" value="' . $datos['UnidadxEntera'] . '"  id="txtUnidxEnteraPrd' . $cont . '" /></td>');
									echo('<td width="10%"><button type="button" class="btn btn-success" onclick=editaCambiosProforma(' . $numProforma . ',"' . $tipo . '",' . $cont . ')><i class="fa fa-plus"></i></button></td>');
									echo('</tr>');
									break;
							}
							$cont ++;
						}
						echo('</tbody></table>
					</div>
				</div>');
			}else{
				echo '<p class="mensaje"><b>No existen Coincidencias para: </p>';
				echo '<center class="respuesta"> '  . $criterio .    '</center>';
			}
		}else{
			echo('<center><p class="fuenteMovil">No existen registros.</p></center>');
		}
	?>							
</body>
</html>