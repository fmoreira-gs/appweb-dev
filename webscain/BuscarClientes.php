<?php
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$criterio = $_GET['criterio'];
	$parametro = $_GET['parametro'];
	$tipoBusqueda = $_GET['tipobusqueda'];
	$local = $_SESSION['log_LOCAL'];

	//VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
    $ver->VerificarSCAIIN('index.php',$local);
	
	$cont=1;
	if(!isset($_SESSION['log_USUARIO'])){
	    header('location:/final/index.php');
	}
	
?>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
		<!--  ESTILOS  -->
		<link rel="stylesheet" type="text/css" href="css/estilos.css" />
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
	<body>
		<?php 
			switch($tipoBusqueda){
				case 'saldos' :
					$clientes = $cone -> ConsultaClientesFacturas($criterio,$parametro,$local);
					break;
				case 'salida':
					$clientes = $cone -> ConsultaSalidaClientes($criterio,$parametro,$local);
					break;
				case 'total':
					$clientes = $cone -> consultaTotalClientes($criterio,$parametro,$local);
					break;
				case 'proforma':
					$clientes = $cone -> consultaTotalProformas($criterio,$parametro,$local);
					break;
				case 'cliente':
					$clientes = $cone -> consultaTotalClientes($criterio,$parametro,$local);
					break;
			}
			
			if ($clientes == null){
				//echo '<h1>No existen Coincidencias para: '  . $parametro .    '</h1>';
				echo '<p class="mensaje"><b>No existen Coincidencias para: </p>';
				echo '<center class="respuesta"> '  . $parametro .    '</center>';
			}else{
				switch ($tipoBusqueda){
					case 'saldos':
						echo "<br><div id='clientes_tabla'><table rules='all' border=1>";
						echo '<tr style="background-color:#00a0d0;">';
						echo '<td>Nombre Cliente</td>';
						echo '<td>Valor</td>';
						echo '<td>Fecha</td>';
						echo '<td>Fact.</td>';
										
						foreach ($clientes as $clientefact) {
							echo '<tr>';
							echo '<td width="25%"><input type="text" class="cajatext" name="facturaefectivo' . $cont . '" id="facturaefectivo' . $cont . '" value="' . $clientefact[CLIENTE] .'" readonly></td>';
							//echo '<td width="20%"><input type="tel" class="cajatext" name="valorefectivo' . $cont . '" id="valorefectivo' . $cont . '" value="' . $detfectivo[Valorcobro] .'" onkeyup="format(this)" onchange="format(this)"></td>';
							echo '<td width="5%"><input type="number" class="cajatext" name="detalleefectivo' . $cont . '" id="detalleefectivo' . $cont . '" value="' . $clientefact[Saldo] . '" maxlength="10" readonly ></td>';
							echo '<td width= "5%"><input type="text" class="cajatext" name="fechafactura' . $cont . '" id="fechafactura' . $cont . '" value="' . date('m-Y',strtotime($clientefact[Fecha])) . '" readonly></td>';
							echo '<td width= "5%"><input type="number" class="cajatext" name="cuotaefectivo' . $cont . '" id="cuotaefectivo' . $cont . '" value="' . $clientefact[Facturas] .'" readonly></td>';
							$cont++;
						}
						
							echo '</tr></table></div><br>';
						break;
					case 'salida':
						echo "<br><div id='datossalida'><table rules='all' border=1>";
						echo '<tr style="background-color:#00a0d0;">';
						echo '<td>NOMBRE_CLIENTE</td>';
						echo '<td>Cod. F</td>';
						echo '<td>Cod. Mon</td>';
						//echo '<td>Cancelacion</td>';
		
						foreach ($clientes as $clientefact) {
							echo '<tr>';
							echo '<td width="20%"><input type="text" class="textobserv" id="nomCliente' . $cont . '" value="' . $clientefact[Nombre] .'" readonly></td>';
							echo '<td width="10%"><input type="number" class="textobserv" id="codCliente' . $cont . '" value="' . $clientefact[Codigo_Cliente] . '" maxlength="10" readonly ></td>';
							echo '<td width= "5%"><input type="text" class="textobserv" id="codMonitoreo' . $cont . '" value="' . $clientefact[Codigo_Monitoreo] . '" readonly></td>';
							echo '<input type="hidden" class="textobserv" id="nomFacturacion' . $cont . '" value="' . $clientefact[NombreFact] . '" readonly>';
							echo '<td width="4%"><center><a href="#" id="Baja" class="Boton_Editar icon_enviar" onClick=cancelaServicio(\'' . $clientefact[Codigo_Cliente] . '\',' . $cont . ',\'' . $clientefact[Codigo_Monitoreo] . '\') ></a></center></td></tr>';
							$cont++;
						}
						echo '</table></div><br>';
						break;			
					case 'total':
						echo ('
						<div id="cheque">
							<br><div id="datosCliente" class="table-responsive">
							<form id="frmNuevaProforma" method="POST">
							<table class="table table-striped" border=1>
								<thead class="thead-dark">
									<th>NOMBRE_CLIENTE</th>
									<th>RUC/CC</th>
									<th>Cod. F</th>
								</thead>
								<tbody>
						');
								foreach ($clientes as $clientefact) {
									echo ('<tr>
										<td width="12%"><input type="text" class="textobserv" id="txtNomCliente' . $cont . '" value="' . $clientefact['Nombre'] .'" readonly></td>
										<td width="5%"><input type="number" class="textobserv" id="txtRuc' . $cont . '" value="' . $clientefact['RUC'] . '" maxlength="13" readonly ></td>
										<td width="5%"><input type="number" class="textobserv" id="txtCodCliente' . $cont . '" value="' . $clientefact['Cliente'] . '" maxlength="10" readonly ></td>
										<input type="hidden" class="textobserv" id="txtcontacto' . $cont . '" value="' . $clientefact['Contacto'] . '" readonly>
										<input type="hidden" class="textobserv" id="txtTelefono' . $cont . '" value="' . $clientefact['Teléfono1'] . '" readonly>
										<input type="hidden" class="textobserv" id="txtDireccion' . $cont . '" value="' . $clientefact['Dirección'] . '" readonly>
										<input type="hidden" class="textobserv" id="txtEmail' . $cont . '" value="' . $clientefact['Email'] . '" readonly>
										<td width="2%"><center><button type="button" class="btn btn-sm btn-success" onClick=creaProforma(\'cabecera\',' . $cont . ',0) ><i class="fa fa-plus"></i></button></center></td>
									');
										$cont++;
								}
						echo ('
								</tr>
							</table>
							</form>
							</div>
						</div>
						<br>');
						break;
					case 'proforma':
						echo ('
						<div id="cheque">
							<br><div id="datosCliente" class="table-responsive">
							<form id="frmNuevaProforma" method="POST">
							<table class="table table-striped" border=1>
								<thead class="thead-dark">
									<th>NOMBRE_CLIENTE</th>
									<th>CONTACTO</th>
									<th>RUC/CC</th>
									<th>Cod. F</th>
								</thead>
								<tbody>
						');
								foreach ($clientes as $clientefact) {
									echo ('<tr>
										<td width="12%"><input type="text" class="textobserv" id="txtNomCliente' . $cont . '" value="' . $clientefact['Nombre'] .'" readonly></td>
										<td width="12%"><input type="text" class="textobserv" id="txtContacto' . $cont . '" value="' . $clientefact['Cliente'] .'" readonly></td>
										<td width="5%"><input type="text" class="textobserv" id="txtRuc' . $cont . '" value="' . $clientefact['RUC'] . '" maxlength="13" readonly ></td>
										<td width="5%"><input type="text" class="textobserv" id="txtCodCliente' . $cont . '" value="' . $clientefact['Cliente_act'] . '" maxlength="10" readonly ></td>
										<td width="2%"><center><button type="button" class="btn btn-sm btn-success" onClick=listarProformasxCliente(' . $cont . ') ><i class="fa fa-plus">Ver Proformas</i></button></center></td>
										<div id="listaProf'. $cont .'"></div>
									');
										$cont++;
								}
						echo ('
								</tr>
							</table>
							</form>
							</div>
							</div>
						<br>');
						break;
					case 'cliente':
						echo ('
						<div id="cheque">
							<br><div id="datosCliente" class="table-responsive">
							<form id="frmNuevaProforma" method="POST">
							<table class="table table-striped" border=1>
								<thead class="thead-dark">
									<th>NOMBRE_CLIENTE</th>
									<th>RUC/CC</th>
									<th>Cod. F</th>
								</thead>
								<tbody>
						');
								foreach ($clientes as $clientefact) {
									echo ('<tr>
										<td width="12%"><input type="text" class="textobserv" id="txtNomCliente' . $cont . '" value="' . $clientefact['Nombre'] .'" readonly></td>
										<td width="5%"><input type="number" class="textobserv" id="txtRuc' . $cont . '" value="' . $clientefact['RUC'] . '" maxlength="13" readonly ></td>
										<td width="5%"><input type="number" class="textobserv" id="txtCodCliente' . $cont . '" value="' . $clientefact['Cliente'] . '" maxlength="10" readonly ></td>
										<input type="hidden" class="textobserv" id="txtcontacto' . $cont . '" value="' . $clientefact['Contacto'] . '" readonly>
										<input type="hidden" class="textobserv" id="txtTelefono' . $cont . '" value="' . $clientefact['Teléfono1'] . '" readonly>
										<input type="hidden" class="textobserv" id="txtDireccion' . $cont . '" value="' . $clientefact['Dirección'] . '" readonly>
										<input type="hidden" class="textobserv" id="txtEmail' . $cont . '" value="' . $clientefact['Email'] . '" readonly>
										<td width="2%"><center><button type="button" class="btn btn-sm btn-success" onClick=editaCambiosProforma(txtNumPof.value,\'cliente\','. $cont .')><i class="fa fa-plus"></i></button></center></td>
									');
										$cont++;
								}
						echo ('
								</tr>
							</table>
							</form>
							</div>
						</div>
						<br>');
						break;
				}
			}
			//date('d-m-Y',strtotime($clientefact[Fecha]))
		?>
	<script type="text/javascript" src="js/script.js"></script> 
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../js/select2.js"></script>
</body>
</html>
