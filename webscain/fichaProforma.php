<?php 

	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

    $local = $_SESSION['log_LOCAL'];
    //VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
    $ver->VerificarSCAIIN('index.php',$local);
    $usuario = $_SESSION['log_IDUSUARIO'];
    
    $numProforma = $_GET['num'];
 
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
      <!-- Bootstrap core CSS -->    
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/select2.css">
		<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="js/script.js"></script> 
        <script type="text/javascript" src="../js/jquery-3.4.1.js"></script>
		<script type="text/javascript" src="../js/select2.js"></script>
  </head>
  <body>
    <main role="main" class="container-fluid">
        <h3><center><strong>FICHAS TÉCNICAS</strong></center></h3>
    <?php
    $fichaTec = $cone->fichaTecnica();
    foreach ($fichaTec as $ftec) {
    echo('
        <br>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead class="table-primary">
                    <tr class="active">
                        <th width=20%"><a href="#" class="btn btn-ligth" onclick=insertaFicha(' . $numProforma . ',' . $ftec['Numero'] . ')>'. $ftec['Promocion'] .'</a></td>
                    </tr>
                </thead>
                <tbody>');
                    echo'<tr><td><strong>Ficha # : '. $ftec['Numero'] .'</strong></td></tr>';
                    $fichaDetalle = $cone->fichaTecDetalle($ftec['Numero']);
                    foreach($fichaDetalle as $fichaDet){
                        echo '<tr><td>' . $fichaDet['nombre'] . '</td></tr>';
                    }
                echo('
                    <tr><td><strong>Iva:   '. $ftec['totalIva'] .'</strong></td></tr>
                    <tr><td><strong>Total: '. $ftec['Total'] .'</strong></td></tr>
                </tbody>
            </table>
        </div>
    ');
    }
    ?>
    <center><a href="#" class="btn btn-primary" onClick=proformas(<?php echo $numProforma ?>) >Cancelar</a></center>
</body>
</html>
