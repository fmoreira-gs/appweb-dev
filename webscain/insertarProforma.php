<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

	$empresa = Config::$Empresa;
	$local = $_SESSION['log_LOCAL'];
	$cajaP = Config::$Caja;

	//VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
	$ver->VerificarSCAIIN('index.php',$local);
	$usuario = $_SESSION['log_IDUSUARIO'];
	$idEmpleado = $_SESSION['log_IDEMPLEADO'];
	$tipo = filter_input(INPUT_POST,'tipo');

	switch ($tipo) {
		case 'cabecera':
			$numero = $cone->UltimoNumero($local, 2);
			$numero = $numero['0']['Numero'];
			$numProforma = $numero + 1;

			$nomCliente = $_POST['nomCliente'];
			$contacto = $_POST['contacto'];
			$codCliente = $_POST['codCliente'];
			$telefono = $_POST['telefono'];
			$ruc = $_POST['ruc'];
			$direccion = $_POST['direccion'];
			$email = $_POST['email'];
			if($email == 'undefined'){
				$email = '';
			}
			if($contacto == 'undefined'){
				$contacto = '';
			}
			if($codCliente == 'undefined'){
				$codCliente = '';
			}

			$datos = "'$empresa','$local','$numProforma',GETDATE(),GETDATE(),'$contacto','$codCliente',0,'$nomCliente','$telefono',98,
					1,0,NULL,0,1,0,0,GETDATE(),GETDATE()+15,GETDATE(),'$usuario','$ruc','$direccion',0,'$idEmpleado',0,'$email',1";

			echo $cone->InsertaNuevaProforma($tipo, $datos);

			break;
		case 'duplicar':

			$numProforma = filter_input(INPUT_POST,'numProforma');
		
			echo $cone->duplicaProforma($numProforma, $usuario, $local);
		
			break;
		case 'servicio':
			$numProforma = filter_input(INPUT_POST,'numProforma');
			$idProducto = filter_input(INPUT_POST,'idProducto');
			$unidadxEntera = filter_input(INPUT_POST,'Xentera');
			$precioB = filter_input(INPUT_POST,'precioB');
			$precioEntera = number_format($precioB * $unidadxEntera,2,'.','');
			$IVA = ($precioB * 1) * 0.12;

			$datos = "'$empresa','$local','$numProforma','$idProducto',0,1,'$precioB',0,NULL,12,'$precioEntera',
					'$IVA',0,NULL,NULL,'$unidadxEntera'";

			echo $cone->InsertaNuevaProforma($tipo, $datos);
			
			$cone->calcularNuevoRegistro($numProforma, $local, $precioB,'ingreso');

			break;

		case 'produccion':
			$numProforma = filter_input(INPUT_POST,'numProforma');
			$idProducto = filter_input(INPUT_POST,'idProducto');
			$unidadxEntera = filter_input(INPUT_POST,'Xentera');
			$precio = filter_input(INPUT_POST,'precio');
			$precioUnidad = ($precio / $unidadxEntera);
			$precioEntera = number_format($precioUnidad * $unidadxEntera,2,'.','');
			$IVA = ($precioUnidad * 1) * 0.12;

			$datos = "'$empresa','$local','$numProforma','$idProducto',0,1,'$precioUnidad',0,NULL,12,'$precioEntera',
					'$IVA',0,NULL,NULL,'$unidadxEntera'";

			echo $cone->InsertaNuevaProforma($tipo, $datos);

			$cone->calcularNuevoRegistro($numProforma, $local, $precioUnidad,'ingreso');

			break;
	}

	

?>