<?php 
	// index de login webscaiin.
	//ARCHIVOS DE CONFIGURACION GLOBAL
	
	require_once __DIR__ . '../../../app/Config.php';
	require_once __DIR__ . '../../../app/Model.php';
			
	//VERIFICAR SOLICITUD DE DATOS
	$local = $_GET['local'];	
	$pagina = $_GET['url'];
	$accion = $_GET['action'];
	$sistema = $_GET['sistema'];
	$usuario = $_POST['usuario'];
	$clave = $_POST['clave'];
	$gps = $_POST['posicion'];
	$latlon = $_POST['latitudlongitud'];

	if(isset($accion)&&$accion=='login'){
	    switch ($x->Autenticar($usuario,$clave,$gps,$latlon,$sistema,$local)) {
	        case 0:
	            $mensaje = 'Usuario o contrase&ntilde;a no han sido validados. Intente nuevamente.';
	            break;
	        case 1:
	            if ($pagina == null){
	                header('location: ../index.php');
	            }else{
	                header('location: ../' . $pagina);
	            }
	            break;
			case 2:
				$clavemd5 = md5($clave);
				echo '<input type="hidden" id="txtClaveMD5" value="' . $clavemd5 . '" />';
				echo '<input type="hidden" id="txtUsuario" value="' . $usuario . '" />';
				echo '<input type="hidden" id="txtClave" value="' . $clave . '" />';
				echo '<input type="hidden" id="txtSistema" value="' . $sistema . '" />';
				echo '<input type="hidden" id="txtLocal" value="' . $local . '" />';
				$mensaje = 'Su contrase&ntildea Ha caducado por favor Comuniquese con el Departamento de Sistemas.!!
				<a class="btn btn-link" href="#" onclick=pasaValores(\''. $clave .'\',\''. $usuario .'\',\''. $clavemd5 .'\',\'scaiin\',' . $local . ') ><strong>Cambie su Contraseña aquí..</strong></a>';

				//VENTANA EMERGENTE
				echo('<div id="cambioContrasenia" class="ventana" title="Facturas" style="display: none"></div>');
				echo('<div id="Block" style="display: none"></div>');
	            break;
	        case 3:
	            $mensaje = 'Su Usuario ha sido BLOQUEADO, favor Comuniquese con el Departamento de Sistemas.!!';
	            break;
	    }
	}
?>
<html>
    <head>
        <meta http-equiv="Content-type" content="initial-scale=1.0; user-scalable=yes; charset=utf-8" />
        <title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
        <link rel="stylesheet" type="text/css" href="../../web/css/estilo.css" />
		<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-3.4.1.js"></script>
		<script type="text/javascript" src="../js/script.js"></script>
        <script type="text/javascript">
			//AJAX PARA CARGAR CONTENIDO
			var xmlhttp;
			var miPosicion;
			var destinoAJAX;
			
			//FUNCION EXCLUSIVA PARA POSICIONAMIENTO
			function mostrarPosicion(position) {
				latitudlongitud.value = position.coords.latitude + '|' + position.coords.longitude;
				miPosicion.value = 'https://www.google.com/maps/place//@' + position.coords.latitude + ',' + position.coords.longitude + ',15z/data=!4m2!3m1!1s0x0:0x0';     
        }
        
        function obtenerPosicion(destino) {
        	 miPosicion = document.getElementById(destino);
        	 miPosicion.value="NO";
        	 if (navigator.geolocation){		 
              navigator.geolocation.getCurrentPosition(mostrarPosicion);
           }else{ 
        	    miPosicion.innerHTML = "NO";
        	 }
		}
	</script>
	</head>
    <body class="FondoAzul" onLoad="obtenerPosicion('posicion');">
    <center>
        <div id="logo"></div>
            <?php 
            	echo '<form name="autenticar" action="../logout/index.php" method="post">';
            		$sucursal = $SD->agencias();
            		foreach($sucursal as $agencia){
            		    if(isset($agencia[Tipo_Cliente]) && ($agencia[Tipo_Cliente]==$local) ){
            				echo '<input type="text" name="sucursal" id="sucursal" class="CajaAgencia" value="Ingreso seleccionado, Agencia '. $agencia[Descripcion] . '"  readonly>';
            			}
            		}
            		echo '<a href=".././index.php"><input type="button" value="Cambio de Agencia" class="BotonAgencia azul Extendido"></a>';
            	echo '</form>';
            ?>
            <div id="contenedor">
            Ingrese su usuario y contrase&ntilde;a para acceder al Sistema Web.
            <br/>
            <center>
            <form name="autenticar" action="index.php?action=login&sistema=SCAIIN&local=<?php echo $local; ?>" method="post">
            <input type="text" name="usuario" class="CajaLogin User" id="cajaUsuario" required />
            <input type="password" name="clave" class="CajaLogin Password" id="cajaPwd" required/>
            <input type="hidden" name="posicion" id="posicion" value="NO"/>
            <input type="hidden" name="latitudlongitud" id="latitudlongitud" value="NO"/>
            <br />
            <input type="reset" value="Borrar" class="Boton gris" />
            <input type="submit" value="Ingresar" class="Boton azul"/>
            </form>
            </center>
        	<?php 
        		//CODIGO GENERADO
        		if(isset($accion)){
        			echo('<span id="mensaje" class="Mensaje" id="mensajeInfo">' . $mensaje . '</span>');
        		}
        	?>
        </div>
    </center>
    </body>
</html>