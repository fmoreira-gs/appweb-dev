<?php
    //ARCHIVOS DE CONFIGURACION GLOBAL
    require_once __DIR__ . '../../app/Config.php';
    require_once __DIR__ . '../../app/Model.php';
    
    //VERIFICAR SI ESTA AUTENTICADO
    if(!isset($_SESSION['log_USUARIO'])){
        header('location:/final/index.php');
    }
    
    $idUsuario = $_GET['usuario'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
?>
<html>
    <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title><?php echo(Config::$mvc_titulo); ?></title>
    <!--  ESTILOS  -->
    <link rel="stylesheet" type="text/css" href="css/estilos.css" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    </head>
    <body><div id="contenedor">
    	<?php
            echo('<div id="Titulo">ASIGNADAS A: ' . $_SESSION['log_USUARIO'] . '<br /><span id="Cuenta"></span></div>'); 
            echo'<div id="ingresoGestionCitas" class="ventana" title="GestionCitas" style="display: none; margin-top: -2em"></div>';
            echo'<div id="Block" style="display: none"></div>';
            echo '<div id="rangoFechas"><b> <p>SELECCIONE UN RANGO DE FECHA <br>';
            echo 'Desde: <input type="date" id="dtpDesde" class="txtRangoFecha" value=' . $desde .'>    ';
            echo 'Hasta: <input type="date" id="dtpHasta" class="txtRangoFecha" value=' . $hasta .'>  <input type="button" class="BotonRangoFecha" onclick="ingresoGestionCitas(\'' . $idUsuario .'\')" value="Cargar">';
            echo '</div>';
        	//CONTENIDO - MANTENIMIENTOS
            $citas = $art ->citasAsignadas($idUsuario,$desde,$hasta);
    		if(count($citas)>0){
    			echo('<center><P><b>Registros encontrados: ' . count($citas) . '</center>');
    			echo('<table class="VistaDatosCitas" >');	
                    echo '<tr>';
                    echo '<td><center><b>IDCLI</center></td>';
                    echo '<td><center><b>CLIENTE</center></td>';
                    echo '<td><center><b>CITA / INI - FIN</center></td>';
                    //echo '<td><center><b>GESTION</center></td>';
                    echo '</tr>';
    			foreach ($citas as $nombres){
    			    echo '<tr id="tr_' . $nombres[MDECIT_IDCITA] . '" >';				
    			    echo '<td width="10%" class="noBorderLeft"><a onClick="DatosClientes(\'' . $nombres[MDECLI_IDCLIE] . '\')">' . $nombres[MDECLI_IDCLIE] . '</a></td>';
    			    echo '<td width="40%"><div class="MasInfo" onclick="MasInfo(\'' . $nombres[MDECIT_IDCITA] . '\')">' . $nombres[CLIENTE] . '</div><div class="MasInfo_detalle" onclick="MasInfo(\'' . $nombres[MDECIT_IDCITA] . '\')" id="' . $nombres[MDECIT_IDCITA] . '" style="display: none"><hr width="50%" />';
    			    echo  '<a onClick="tipoGestion(\'' . $nombres[MDECIT_IDCITA] . '\',\'M\',\'' . $idUsuario . '\')">' . $nombres[MDECIT_IDCITA] . ' - ' . $nombres[MDECIT_DIRECC] . '<hr width ="50%">' . $nombres[MDECIT_REQUER] . '</a>';	
    			    echo '</div></td>';
    			    echo '<td width="25%">' . date('d-m-Y', strtotime($nombres[MDECIT_FECHAV])) . ' /<b> ' . date('H:i', strtotime($nombres[MDECIT_HORAIN])) . ' - ' . date('H:i', strtotime($nombres[MDECIT_HORAFI])) . '</b></td>';
    				//echo '<td width="10%" class="noBorderRight"><a onClick="Historial(\'' . $nombres[MDECLI_IDCLIE] . '\')"><img src="images/historial.png" class="iconoCelda" alt="Historial" title="Historial"></a></td>';
    				echo '<td width="10%"><a onClick="gestionCitas(\'' . $nombres[MDECIT_IDCITA] . '\',\'C\',\'' . $idUsuario .'\')"><img src="images/gestion.png" class="iconoCelda" alt="Gestion" title="Gesti&oacute;n"></a></td>';
    				echo '</tr>';
    			}
    			echo('</table>');
    		}else{
    			echo('<center>No se encontraron registros.</center>');
    		}
        ?>
    </div></body>
</html>