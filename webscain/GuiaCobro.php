<?php 
	
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';
	
	$local = $_SESSION['log_LOCAL'];
	//VERIFICAR SI ESTA AUTENTICADO
	$ver = new Login();
	$ver->VerificarSCAIIN('index.php',$local);
	$usuario = $_SESSION['log_IDUSUARIO'];
	
	
?>
<html>
    <head>
    	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    	<title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
    	<!--  ESTILOS  -->
    	<link rel="stylesheet" type="text/css" href="css/estilos.css" />
    	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
    	<script type="text/javascript" src="js/script.js"></script>
    </head>
<body><div id="contenedor">

<?php
	$num = $_GET['guia'];
	//CONDICIÓN PARA ASIGNAR UN NUMERO A LA VARIABLE 'num' SI NO TITNE NINGUN  VALOR O VALOR INDEFINIDO DESDE EL METODO GET
	if($num == null || $num == 'undefined'){
	    $numguias = $cone->UltimoNumero1($usuario,$local);
		$num = $numguias[0][Numero];
		if($num == null || $num=='undefined'){
		    $numguias = $cone->UltimoNumero($local,1);
			$num = $numguias[0][Numero];
		}
	}
	
	$numero = urldecode($_POST['numguia']);
	echo '<div id="formulario">';
	if($numero==null){
		$numero = $num;
		$valortext= $num;
	}else{
		$valortext=$_POST['numguia'];
	}
	echo '<input type="button" onClick=anterior(' . $valortext . ') class="botonant" name="ant" id="ant" value="<" >';
	echo '<input type="text" onchange=BuscaGuia(this) class="textbox" min="1" max=' . $num . '  id="numguia" value="' . $valortext . '" />';
	echo '<input type="button" onClick=siguiente(' . $num . ') class="botonsig"   name="sig" id="sig" value=">" />';
	echo '</div><br>';
	
	$numfactura = $_GET['numfac'];
	$valor = $_GET['valor'];
	
	echo '<input type="hidden" id=""numorden"" value="' . $numero . '" ></td>';
	$guias= $cone->GuiasCobro($numero, $local);
	$empleado = $guias[0][Empleado];
	
	if ($guias != null) {
		// ENCABEZADO  --------------------------
		$estadoguia = $guias[0][Estado];
		if ($estadoguia == 'Pasivo'){
    		echo '<div>';
    		echo '<a target="blank" href="Reportes/rpt_GuiaCobro.php?guia=' . $numero .'&local='. $local .'" class="logoimprimir"></a>';
    		echo '</div>';
		}
		switch ($local){
		    case 1:
		        $sucursal = 'SANTO DOMINGO';
		        break;
		    case 2:
		        $sucursal = 'QUITO';
		        break;
		    case 3:
		        $sucursal = 'ESMERALDAS';
		        break;
		    case 4:
		        $sucursal = 'CONCORDIA';
		        break;
		    case 6:
		        $sucursal = 'QUEVEDO';
		        break;
		    case 8:
		        $sucursal = 'LAGO AGRIO';
		        break;
		    case 12:
		        $sucursal = 'MANABI';
		        break;
		    case 13:
		        $sucursal = 'PORTOVIEJO';
		        break;
		}
		
		if($guias[0][origen] == 1){
		    $origen = 'WEB';
		}elseif($guias[0][origen] == 0){
		    $origen = 'OFICINA';
		}
		echo '<div class=""><b>ORIGEN: </b>  '. $origen .'';
        echo '<br><b>LOCAL ACTUAL:</b>  '. $sucursal .'</div>';
		echo "<div class='menu_acordeon'>";
		echo "<div class='titulo'>";
		echo '<p class="tutulo">Informaci&oacuten Guia	-	 <label class="totalmenu">'. $estadoguia .' </label></p>';
		echo "</div>";
		echo "<div class='info'>";
		echo '<div id="encabezado"><table rules="all" border=1 >';
        	echo '<tr><td><b>Fecha: <input type="text" class="textboxencabezado" id="fecha" name="fecha" value=\'' . date('Y-m-d',strtotime($guias[0][Fecha])) . '\' readonly="readonly"> Hora: <input type="text" class="textboxencabezado" id="hora" name="hora" value=\'' . date('H:i',strtotime($guias[0][Hora])) . '\' readonly="readonly"></td></tr>';
        	echo '<tr><td><b>Recaudador:</b> ' . $guias[0][empleadonombre] . $agencia . '</td></tr>';
        	echo '<tr><td><b>Estado:</b> ' . $estadoguia . '</td></tr>';
        	echo '<tr><td><b>Creado Por:</b>  ' . $guias[0][usuario] . ' </td></tr>';
        	echo '<tr><td><b>Recibo de Caja:</b>  ' . $guias[0][RecibodeCaja] . '</td></tr>';
        	echo '<tr><td><b>Creacion R.C.:</b>  ' . $guias[0][Creacion_ReciboCaja] . '</td></tr>';
        	echo '<tr><td></td></tr>';
		echo '</tr></table><hr></div>';
		$pendiente = $cone->GuiaPendiente($usuario,$local);
			if ($pendiente != null ){
				echo '<font color="red"><b>Usted tiene la(s) Siguiente(s) guia(s) Pendiente(s): ';
				foreach ($pendiente as $guiapendiente){
					echo $guiapendiente[Numero] . ', ';
				}
				echo 'Por lo tanto no Puede crear una nueva.</b></font>';
			}else{
				echo '<center><a href="#" class="Vinculo icon_agregar" onclick=NuevaGuiaCobro(' . $numero . ');> Nueva Guia</a></center>';
			}
			echo '</div></div>';
		
		// **************************************
		// EFECTIVO *-------------------------------
		$totaldetalle = $cone->totaldetalle($numero,"efectivo",$local);
		$totefectivo = number_format($totaldetalle[0][total], 2, '.', '');
		
		echo "<div class='menu_acordeon'>";

			echo "<div class='titulo'>";
				echo "<p class='tutulo'>Detalle Efectivo	-	 <label class='totalmenu'>Suman: $ $totefectivo</label></p>";
			echo "</div>";
		
		echo "<div class='info'>";
		$efectivo = $cone->detalle($numero,$local);
			echo "<br><div id='efectivo'><table rules='all' border=1>";
			echo '<tr style="background-color:#00a0d0;">';
			echo '<td>Factura</td>';
			echo '<td>Valor</td>'; 
			echo '<td>Cuota</td></tr>';
			$cont=1;
			foreach ($efectivo as $detfectivo) {
				echo '<tr>';
				echo '<td width="20%"><input type="number" class="cajatext" name="facturaefectivo' . $cont . '" id="facturaefectivo' . $cont . '" value="' . $detfectivo[Factura] .'" readonly></td>';
				//SE LE PONE DE TIPO "TEL" PARA QUE LA FUNCION FUNCIONE A PERFECCION YA QUE CON NUMBER NO PERMIT�A INGRESAR ALGUNAS CANTIDADES DE MANERA CORRECTA
				echo '<td width="20%"><input type="tel" class="cajatext" name="valorefectivo' . $cont . '" id="valorefectivo' . $cont . '" value="' . $detfectivo[Valorcobro] .'" onkeyup="format(this)" onchange="format(this)"></td>';
				echo '<td width= "5%"><input type="number" class="cajatext" name="cuotaefectivo' . $cont . '" id="cuotaefectivo' . $cont . '" value="' . $detfectivo[Cuota] .'" readonly></td>';
				echo '<input type="hidden" name="cajaefectivo' . $cont . '" id="cajaefectivo' . $cont . '" value="' . $detfectivo[Caja] . '" ></td>';

				if($guias[0][Estado]==Actualizada || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
				    $factura = $cone->saldofactura($detfectivo[Cuota], $detfectivo[Factura], $detfectivo[Caja],$local);
					if($detfectivo[Cuota] == 0){
						$saldof= 'saldoFactura';
					}else{
						$saldof = 'saldovalor';
					}
				echo '<td width="10%"><a href="#" id="Editar" class="Boton_Editar icon_editar" onClick=efectivo(this,' . $factura[0][$saldof] . ',' . $cont . ',' . $detfectivo[row] . ') ></a></td>';
				echo '<td width="10%"><a href="#" id="Eliminar" class="Boton_Eliminar icon_eliminar" onClick=eliminaregistro(' . $detfectivo[Factura] . ',' . $detfectivo[Cuota] . ',' . $numero . ',"efectivo",' . $detfectivo[Caja] . ',' . $detfectivo[row] . ')></a></td>';
				}

				//echo '<td width="10%"><a href="javascript:window.open(\'ReciboPago.php?factura=' . $detfectivo[Factura] . '&fpago=efectivo&cuota=' . $detfectivo[Cuota] .'&guia=' . $numero . '&saldo_ant=' . $factura[0][$saldof] . '&usactivo='. $_SESSION['log_USUARIO'] . '\')" target="blank" >Imprimir</a></td>';
				//echo '<td width="10%"><a href="javascript:window.open(\'pdf/pdf.php?factura=' . $detfectivo[Factura] . '&fpago=efectivo&cuota=' . $detfectivo[Cuota] .'&guia=' . $numero . '&saldo_ant=' . $factura[0][$saldof] . '&usactivo='. $_SESSION['log_USUARIO'] . '\')" target="blank" >Imprimir</a></td>';
				//echo '<td width="10%"><a href="javascript:window.open(\'pdf/pdf2.php?factura=' . $detfectivo[Factura] . '&fpago=efectivo&cuota=' . $detfectivo[Cuota] .'&guia=' . $numero . '&saldo_ant=' . $factura[0][$saldof] . '\')" >Imprimir</a></td>';
				//echo '<td width="10%"><a href="#" id="imprimir" class="Boton_Editar icon_editar" onClick=imprimir_recibo(' . $detfectivo[Factura] . ',"efectivo",' . $cont . ')></a></td>';
				$cont++;
			}
			
			echo '</tr></table></div><br>';
			if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
				echo '<center><a href="#" id="vinculoFactura" class="vinculo icon_agregar" onClick=FacturasP(' . $numero . ',"efectivo")> Nuevo Efectivo</a></center>';	
			}
			echo('<div id="facturasPendientes" class="ventana" title="Facturas" style="display: none"></div>');
			echo('<div id="Block" style="display: none"></div>');
			echo ('</div></div>');
		// ***************************************		
		// CHEQUE ------------------------------
		$totaldetalle = $cone->totaldetalle($numero,"cheque",$local);
		$totcheque = number_format($totaldetalle[0][total], 2, '.', '');
		
		echo "<div class='menu_acordeon'>";
			echo "<div class='titulo'>";
				echo "<p class='tutulo'>Detalle Cheques	-	<label class='totalmenu'>Suman: $ $totcheque</label></p>";
			echo "</div>";
			echo "<div class='info'>";
				$hoy = date('d')+1 . '-' . date('m'). '-' . date('Y');
				$hoy = date('Y-m-d',strtotime($hoy));
				$cheque = $cone->cheques($numero,$local);
				echo '<br><div id="cheque"><table rules="all" border=1>';
				echo '<tr style="background-color:#00a0d0;">';
				echo '<td>Factura</td>';
				echo '<td>Valor</td>';
				echo '<td>Cuota</td>';
				echo '<td>Banco</td>';
				echo '<td>Cheque</td>';
				echo '<td>Cuenta</td>';
				echo '<td>Posf</td>';
				echo '<td>FechaPosf</td></tr>';
				$cont=1;
				foreach ($cheque as $detcheque) {
					$bancos = $cone->bancos();
					echo '<tr>';
					echo '<td width="10%"><input type="number" class="cajatext"  name="facturacheque' . $cont . '" id="facturacheque' . $cont . '" value="' . $detcheque[Factura] . '" readonly></td>';
					//SE LE PONE DE TIPO "TEL" PARA QUE LA FUNCION FUNCIONE A PERFECCION YA QUE CON NUMBER NO PERMIT�A INGRESAR ALGUNAS CANTIDADES DE MANERA CORRECTA
					echo '<td width="10%"><input type="tel" class="cajatext"  name="valorcheque' . $cont . '" id="valorcheque' . $cont . '" value="' . $detcheque[Valor] . '" onkeyup="format(this)" onchange="format(this)"></td>';
					echo '<td width="5%"><input type="number" class="cajatext"  name="cuotacheque' . $cont . '" id="cuotacheque' . $cont . '" value="' . $detcheque[Cuota] . '" readonly ></td>';
					echo '<td width="20%"><select class="cajatext" name="bancocheque' . $cont . '" id="bancocheque' . $cont . '">';
							echo '<option>---</option>';
							foreach($bancos as $banco){
								echo '<option value="' . $banco[Codigo] . '"';
								if(isset($detcheque[banco]) && ($detcheque[banco]==$banco[Banco]) ){
									echo ('selected');
								}
							echo '>';
							echo $banco[Banco];	
							echo '</option>';
							}
							echo '</select>'; 
					echo '<td width="10%"><input type="number" class="cajatext"  name="numcheque' . $cont . '" id="numcheque' . $cont . '" value="' . $detcheque[Cheque] . '" maxlength="10" onchange=solonumeros(this); ></td>';
					echo '<td width="15%"><input type="number" class="cajatext"  name="cuentacheque' . $cont . '" id="cuentacheque' . $cont . '" value="' . $detcheque[cuenta] . '" maxlength="15" onchange=solonumeros(this);></td>';
					
				if($detcheque[Posfecha]==1){
				        echo '<td 	><center><a class="Radio check_ON" onClick=check(' . $cont . ') id="H'. $cont . '"></a>';
				        echo('<input type="checkbox" style="display: none" checked name="chequepos' . $cont . '" id="chequepos' . $cont . '"  value="' . $detcheque[Posfecha] . '"/></center></td>');
					}else{
					    echo '<td width="5%"><center><a class="Radio check_OFF" onClick=check(' . $cont . ') id="H'. $cont . '"></a>';
					    echo('<input type="checkbox" style="display: none" uncheked name="chequepos' . $cont . '" id="chequepos' . $cont . '"  value="' . $detcheque[Posfecha] . '"/></center></td>');
					}					
		
					if($detcheque[fechaP]!=null){
						echo '<td width="5%"><input type="date" class="cajatext" name="fechapos' . $cont . '" id="fechapos' . $cont . '" min="' . $hoy . '" value="' . date('Y-m-d', strtotime($detcheque[fechaP])) . '" ></td>';
					}else{
						echo '<td width="5%"><input type="date" class="cajatext" name="fechapos' . $cont . '" id="fechapos' . $cont . '" min=' . $hoy . '></td>';
					}
					echo '<input type="hidden" name="cajacheque' . $cont . '" id="cajacheque' . $cont . '" value="' . $detcheque[Caja] . '" ></td>';
					echo '<input type="hidden" name="cuotacheque' . $cont . '" id="cuotacheque' . $cont . '" value="' . $detcheque[Cuota] . '" ></td>';
					if($guias[0][Estado]==Actualizada || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
					    $factura = $cone->saldofactura($detcheque[Cuota], $detcheque[Factura],$detcheque[Caja],$local);
						if($detcheque[Cuota] == 0){
							$saldof = 'saldoFactura';
						}else{
							$saldof = 'saldovalor';
						}
						echo '<td width="5%"><a href="#" id="Editar" class="Boton_Editar icon_editar" onClick=cheques(this,' . $factura[0][$saldof] . ',' . $cont . ',' . $detcheque[row] . ') ></a></td>';
						echo '<td width="5%"><a href="#" id="Eliminar" class="Boton_Eliminar icon_eliminar" onClick=eliminaregistro(' . $detcheque[Factura] . ',' . $detcheque[Cuota] . ',' . $numero . ',"cheque",' . $detcheque[Caja] . ',' . $detcheque[row] . ')></a></td>';
					}
					$cont++;
				}
				echo '</tr></table></div><br>';
				if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
					echo '<center><a href="#" id="vinculoFactura" class="vinculo icon_agregar" onClick=FacturasP(' . $numero . ',"cheque")> Nuevo Cheque</a>';
				}
		
		echo "</div></div>";
		
		//*******************************************************
		// TRANSFERENCIAS  ------------------------------
		$totaldetalle = $cone->totaldetalle($numero,"transferencia",$local);
		$tottransferencia = number_format($totaldetalle[0][total], 2, '.', '');
		
		echo "<div class='menu_acordeon'>";
		echo "<div class='titulo'>";
		echo "<p class='tutulo'>Detalle Transferencias	-	<label class='totalmenu'>Suman: $ $tottransferencia</label></p>";
		echo "</div>";
		echo "<div class='info'>";
		$transf = $cone->transacciones($numero,"TR",$local);
			echo '<br><div id="cheque"><table rules="all" border=1>';
				echo '<tr style="background-color:#00a0d0;">';
				echo '<td>Factura</td>';
				echo '<td>Valor</td>';
				echo '<td>Cuota</td>';
				echo '<td>Banco</td>';
				echo '<td>Transf.</td>';
			echo '<td>Fecha</td></tr>';

			$cont=1;
			foreach ($transf as $tr) {
				$bancos = $cone->bancos();
				echo '<tr>';
				echo '<td width="10%"><input type="number" class="cajatext"  name="facturatr' . $cont . '" id="facturatr' . $cont . '" value="' . $tr[factura] . '" readonly></td>';
				//SE LE PONE DE TIPO "TEL" PARA QUE LA FUNCION FUNCIONE A PERFECCION YA QUE CON NUMBER NO PERMIT�A INGRESAR ALGUNAS CANTIDADES DE MANERA CORRECTA
				echo '<td width="10%"><input type="tel" class="cajatext"  name="valortr' . $cont . '" id="valortr' . $cont . '" value="' . $tr[valor] . '" onkeyup="format(this)" onchange="format(this)"></td>';
				echo '<td width="5%"><input type="number" class="cajatext"  name="cuotatr' . $cont . '" id="cuotatr' . $cont . '" value="' . $tr[cuota] . '" readonly ></td>';
				echo '<td width="20%"><select class="cajatext" name="bancotr' . $cont . '" id="bancotr' . $cont . '">';
				echo '<option>---</option>';
				foreach($bancos as $banco){
					echo '<option value="' . $banco[Codigo] . '"';
					if(isset($tr[banco]) && ($tr[banco]==$banco[Banco])){
						echo ('selected');
					}
					echo '>';
					echo $banco[Banco];
					echo '</option>';
				}
				echo '</select></td>';
				echo '<td width="10%"><input type="number" class="cajatext"  name="numtr' . $cont . '" id="numtr' . $cont . '" value="' . $tr[documento] . '" maxlength="10" onchange="solonumeros(this)" ></td>';
				if($tr[fechaTransferencia]!=null){
					echo '<td width="5%"><input type="date" class="cajatext" id="fechatr' . $cont . '" value="' . date('Y-m-d', strtotime($tr[fechaTransferencia])) . '"></td>';
				}else{
					echo '<td width="5%"><input type="date" class="cajatext" id="fechatr' . $cont . '" ></td>';
				}
				echo '<input type="hidden" name="cajatr' . $cont . '" id="cajatr' . $cont . '" value="' . $tr[Caja] . '" >';
					if($guias[0][Estado]==Actualizada || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
					    $factura = $cone->saldofactura($tr[cuota], $tr[factura],$tr[Caja],$local);
						if($tr[cuota] == 0){
							$saldof = 'saldoFactura';
						}else{
							$saldof = 'saldovalor';
						}
						echo '<td width="5%"><a href="#" id="Editar" class="Boton_Editar icon_editar" onClick=transferencias(this,' . $factura[0][$saldof] . ',' . $cont . ',' . $tr[row] . ') ></a></td>';
						echo '<td width="5%"><a href="#" id="Eliminar" class="Boton_Eliminar icon_eliminar" onClick=eliminaregistro(' . $tr[factura] . ',' . $tr[cuota] . ',' . $numero . ',"transferencia",' . $tr[Caja] . ',' . $tr[row] . ')></a></td>';
					}
					$cont++;
			}
			echo '</tr></table></div><br>';
			if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
			echo '<center><a href="#" id="vinculoFactura" class="vinculo icon_agregar" onClick=FacturasP(' . $numero . ',"transferencia")> Nueva Transferencia</a>';
		}
		echo "</div></div>";
		
		//*******************************************************
		// TARJETAS DE CREDITO  ------------------------------
		$totaldetalle = $cone->totaldetalle($numero,"tarjeta",$local);
		$tottarjeta = number_format($totaldetalle[0][total], 2, '.', '');
		
		echo "<div class='menu_acordeon'>";
		echo "<div class='titulo'>";
				echo "<p class='tutulo'>Detalle Tarjetas	-	<label class='totalmenu'>Suman: $ $tottarjeta</label></p>";
		echo "</div>";
		echo "<div class='info'>";
		$tarjeta = $cone->transacciones($numero,"TC",$local);
			echo '<br><div id="cheque"><table rules="all" border=1>';
			echo '<tr style="background-color:#00a0d0;">';
			echo '<td>Factura</td>';
			echo '<td>Valor</td>';
			echo '<td>Cuota</td>';
			echo '<td>Banco</td>';
			echo '<td>Boucher</td>';
			echo '<td>Autoriz.</td>';
			echo '<td>N.Tarjeta</td></tr>';
		$cont=1;
		foreach ($tarjeta as $tc) {
		$bancos = $cone->bancosTarjetas();
		echo '<tr>';
		echo '<td width="10%"><input type="number" class="cajatext"  name="facturatc' . $cont . '" id="facturatc' . $cont . '" value="' . $tc[factura] . '" readonly></td>';
		//SE LE PONE DE TIPO "TEL" PARA QUE LA FUNCION FUNCIONE A PERFECCION YA QUE CON NUMBER NO PERMIT�A INGRESAR ALGUNAS CANTIDADES DE MANERA CORRECTA
		echo '<td width="10%"><input type="tel" class="cajatext"  name="valortc' . $cont . '" id="valortc' . $cont . '" value="' . $tc[valor] . '" onkeyup="format(this)" onchange="format(this)"></td>';
		echo '<td width="5%"><input type="number" class="cajatext"  name="cuotatc' . $cont . '" id="cuotatc' . $cont . '" value="' . $tc[cuota] . '" readonly ></td>';
		echo '<td width="20%"><select class="cajatext" name="bancotc' . $cont . '" id="bancotc' . $cont . '">';
		echo '<option>---</option>';
		foreach($bancos as $banco){
			echo '<option value="' . $banco[COD] . '"';
			if(isset($tc[banco]) && ($tc[banco]==$banco[Tarjeta]) ){
				echo ('selected');
			}
			echo '>';
			echo $banco[Tarjeta];
			echo '</option>';
		}
		echo '</select>';
		echo '<td width="10%"><input type="number" class="cajatext"  name="numtc' . $cont . '" id="numtc' . $cont . '" value="' . $tc[documento] . '" maxlength="10" onchange=solonumeros(this); ></td>';
		echo '<td width="15%"><input type="number" class="cajatext"  name="autorizaciontc' . $cont . '" id="autorizaciontc' . $cont . '" value="' . $tc[autorizacion] . '" maxlength="15" onchange=solonumeros(this);></td>';
		echo '<td width="15%"><input type="number" class="cajatext"  name="numerotc' . $cont . '" id="numerotc' . $cont . '" value="' . $tc[numTarjeta] . '" maxlength="15" onchange=solonumeros(this);></td>';
		echo '<input type="hidden" name="cajatc' . $cont . '" id="cajatc' . $cont . '" value="' . $tc[Caja] . '" ></td>';
		echo '<input type="hidden" name="idtc' . $cont . '" id="idtc' . $cont . '" value="' . $tc[tarjeta] . '" ></td>';
		
		if($guias[0][Estado]==Actualizada || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
			$factura = $cone->saldofactura($tc[cuota], $tc[factura],$tc[Caja],$local);
			if($tc[cuota] == 0){
				$saldof = 'saldoFactura';
			}else{
				$saldof = 'saldovalor';
			}
			echo '<td width="5%"><a href="#" id="Editar" class="Boton_Editar icon_editar" onClick=tarjetas(this,' . $factura[0][$saldof] . ',' . $cont . ',' . $tc[row] . ') ></a></td>';
			echo '<td width="5%"><a href="#" id="Eliminar" class="Boton_Eliminar icon_eliminar" onClick=eliminaregistro(' . $tc[factura] . ',' . $tc[cuota] . ',' . $numero . ',"tarjeta",' . $tc[Caja] . ',' . $tc[row] . ')></a></td>';
		}
		$cont++;
		}
		echo '</tr></table></div><br>';
		if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
			echo '<center><a href="#" id="vinculoFactura" class="vinculo icon_agregar" onClick=FacturasP(' . $numero . ',"tarjeta")> Nueva Tarjeta</a>';
		}
		echo "</div></div>";
		//*******************************************************
		
		// RETENCIONES  ------------------------------
		$totaldetalle = $cone->totaldetalle($numero,"retencion",$local);
		$totret = number_format($totaldetalle[0][total], 2, '.', '');
		
		echo "<div class='menu_acordeon'>";
		echo "<div class='titulo'>";
		echo "<p class='tutulo'>Detalle Retenciones	-	<label class='totalmenu'>Suman: $ $totret</label></p>";
		echo "</div>";
		echo "<div class='info'>";
		$retencion = $cone->retenciones($numero,$local);
		echo '<br><div id="cheque"><table rules="all" border=1>';
		echo '<tr style="background-color:#00a0d0;">';
		echo '<td>Factura</td>';
		echo '<td>Valor</td>';
		echo '<td># Retencion</td>';
		echo '<td>Rubro</td>';
		echo '<td>Det. Impuesto</td></tr>';
		$cont=1;
		foreach ($retencion as $rt) {
			$rubros = $cone->tipoRubro();
			$impuestos = $cone-> detalleImpuesto();
			echo '<tr>';
			echo '<td width="10%"><input type="number" class="cajatext"  name="facturart' . $cont . '" id="facturart' . $cont . '" value="' . $rt[factura] . '" readonly></td>';
			//SE LE PONE DE TIPO "TEL" PARA QUE LA FUNCION FUNCIONE A PERFECCION YA QUE CON NUMBER NO PERMIT�A INGRESAR ALGUNAS CANTIDADES DE MANERA CORRECTA
			echo '<td width="8%"><input type="tel" class="cajatext"  name="valorrt' . $cont . '" id="valorrt' . $cont . '" value="' . number_format($rt[valorRubro], 2, '.', '') . '" onkeyup="format(this)" onchange="format(this)"></td>';
			echo '<td width="15%"><input type="number" class="cajatext" id="detallert' . $cont . '" value=' . $rt[numDocumento] . ' maxlength="10" ></td>';
			echo '<td width="25%"><select class="cajatext" name="rubrort' . $cont . '" id="rubrort' . $cont . '">';
			echo '<option>---</option>';
			foreach($rubros as $rubro){
				echo '<option value="' . $rubro[Rubro] . '"';
				if(isset($rt[rubro]) && ($rt[rubro]==$rubro[Rubro]) ){
					echo ('selected');
				}
				echo '>';
				echo $rubro[Descripcion];
				echo '</option>';
			}
			echo '</select>';
			echo '<td width="50%"><select class="cajatext" name="imprt' . $cont . '" id="imprt' . $cont . '">';
			echo '<option> </option>';
			foreach($impuestos as $impuesto){
				echo '<option value="' . $impuesto[CodImp] . '"';
				if(isset($rt[detalleImpuesto]) && ($rt[detalleImpuesto]==$impuesto[Detalle]) ){
					echo ('selected');
				}
				echo '>';
				echo $impuesto[CodImp].' - '.$impuesto[Detalle];
				echo '</option>';
			}
			echo '</select>';
			echo '<input type="hidden" id="cajart' . $cont . '" value="' . $rt[Caja] . '" ></td>';
			echo '<input type="hidden" id="cuotart' . $cont . '" value="' . $rt[cuota] . '" ></td>';


			if($guias[0][Estado]==Actualizada || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
			    $factura = $cone->saldofactura($rt[cuota], $rt[factura],$rt[Caja],$local);
				if($rt[cuota] == 0){
					$saldof = 'saldoFactura';
				}else{
					$saldof = 'saldovalor';
				}
				echo '<td width="5%"><a href="#" id="Editar" class="Boton_Editar icon_editar" onClick=retenciones(this,' . $factura[0][$saldof] . ',' . $cont . ',' . $rt[row] . ') ></a></td>';
				echo '<td width="5%"><a href="#" id="Eliminar" class="Boton_Eliminar icon_eliminar" onClick=eliminaregistro(' . $rt[factura] . ',' . $rt[cuota] . ',' . $numero . ',"retencion",' . $rt[Caja] . ',' . $rt[row] . ')></a></td>';
			}
			$cont++;
		}
		echo '</tr></table></div><br>';
		if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
			echo '<center><a href="#" id="vinculoFactura" class="vinculo icon_agregar" onClick=FacturasP(' . $numero . ',"retencion")> Nuevo Rubro</a>';
		}
		echo "</div></div>";
		//*******************************************************
		
		
		//DETALLE BILLETES---------------
		echo "<div class='menu_acordeon'>";
		echo '<div id="detallebilletes">';
		$totaldetalle = $cone->totaldetalle($numero,"billetes", $local);
		$totbillete = number_format($totaldetalle[0][total], 2, '.', '');
		echo "<div class='titulo'>";
		echo "<div id='titulobilletes'><p class='tutulo'>Detalle Billetes	-	<label class='totalmenu'>Suman: $ $totbillete</label> </p></div>";
		echo "</div>";
				echo "<div class='info' id='info_bille'>";
				$billetes = $cone->billetes($numero,$local);
						?>
								<table rules="all" border=1>
									<tr style="background-color:#00a0d0;">
										<td>Den. Billetes</td>
										<td>Cantidad</td>
										<td>Total</td>
									</tr>
									<tr>
										<td><input type="text" class="textefectivo" id="uno" value=1 readonly></td>
										<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==1){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="unoval" onchange=calcular(this,'<?php echo($numero); ?>',"billetes"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
										<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totaluno" readonly></td>
										<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'billetes','','',1)></a></td></tr>";	}}?>
									</tr>
									<tr>
										<td><input type="text" class="textefectivo" id="cinco" value=5 readonly></td>
										<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==5){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1; }} ?>" id="cincoval" onchange=calcular(this,'<?php echo($numero); ?>',"billetes"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
										<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcinco" readonly></td>
										<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'billetes','','',5)></a></td></tr>";	}}?>
									</tr>
									<tr>
										<td><input type="text" class="textefectivo" id="diez" value=10 readonly></td>
										<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==10){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1; }} ?>" id="diezval" onchange=calcular(this,'<?php echo($numero); ?>',"billetes"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
										<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totaldiez" readonly></td>
										<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'billetes','','',10)></a></td></tr>";	}}?>
									</tr>
									<tr>
										<td><input type="text" class="textefectivo" id="veinte" value=20 readonly></td>
										<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==20){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="veinteval" onchange=calcular(this,'<?php echo($numero); ?>',"billetes"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
										<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalveinte" readonly></td>
										<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){  if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'billetes','','',20)></a></td></tr>";	}}?>
									</tr>
									<tr>
										<td><input type="text" class="textefectivo" id="cincuenta" value=50 readonly></td>
										<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==50){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="cincuentaval" onchange=calcular(this,'<?php echo($numero); ?>',"billetes"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
										<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcincuenta" readonly></td>
										<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'billetes','','',50)></a></td></tr>";	}}?>
									</tr>
									<tr>
										<td><input type="text" class="textefectivo" id="cien" value=100 readonly></td>
										<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($billetes as $billete){if($billete[UnidxBillete]==100){echo $billete[Cantidad]; $total= ($billete[UnidxBillete])*($billete[Cantidad]); $val=1;}} ?>" id="cienval" onchange=calcular(this,'<?php echo($numero); ?>',"billetes"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
										<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcien" readonly></td>
										<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){  if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'billetes','','',100)></a></td></tr>";	}}?>
									</tr>
								</table>
					<?php
					echo "</div></div>";
					echo '</div>';
					//echo '<input id="boton_bille" type="button" value="toggle() con efecto slow" onClick=botonbillete()>';
			
			//**********************************************
			
			//DETALLE MONEDAS-------------------------------
			echo "<div class='menu_acordeon'>";
			echo "<div id='detallemonedas'>";
				$tipo ='monedas';
				$totaldetalle = $cone->totaldetalle($numero,"monedas",$local);
				$totmonedas = number_format($totaldetalle[0][total], 2, '.', '');
				echo "<div class='titulo'>";
				echo "<p class='tutulo'>Detalle Monedas	-	<label class='totalmenu'>Suman: $ $totmonedas</label> </p>";
				echo "</div>";
				echo "<div class='info' id='info_mone'>";
				$monedas = $cone->monedas($numero,$local);
			?>
			<div id="detallemonedas">
				<table rules="all" border="1">
					<tr style="background-color:#00a0d0;">
						<td>Den. Moneda</td>
						<td>Cantidad</td>
						<td> Total </td>
					</tr>
					<tr>
						<td><input type="text" class="textefectivo"  id="unocent" value=0.01 readonly></td>
						<td><input type="number" class="textefectivo"  value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.01){echo ($moneda[Cantidad]); $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="unocentval" onchange=calcular(this,'<?php echo($numero); ?>',"monedas"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
						<td><input type="text" class="textefectivo"  value="<?php if($val!=0){echo$total;}?>" id="totalunocent" readonly></td>
						<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'monedas','','',0.01)></a></td></tr>";	}}?>
					</tr>
					<tr>
						<td><input type="text" class="textefectivo" id="cincocent" value=0.05 readonly></td>
						<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.05){echo ($moneda[Cantidad]); $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="cincocentval" onchange=calcular(this,'<?php echo($numero); ?>',"monedas"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
						<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcincocent" readonly></td>
						<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'monedas','','',0.05)></a></td></tr>";	}}?>
					</tr>
					<tr>
						<td><input type="text" class="textefectivo" id="diezcent" value=0.10 readonly></td>
						<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.10){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="diezcentval" onchange=calcular(this,'<?php echo($numero); ?>',"monedas"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
						<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totaldiezcent" readonly></td>
						<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'monedas','','',0.10)></a></td></tr>";	}}?>
					</tr>
					<tr>
						<td><input type="text" class="textefectivo" id="veinticincocent" value=0.25 readonly></td>
						<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.25){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="veinticincocentval" onchange=calcular(this,'<?php echo($numero); ?>',"monedas"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
						<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalveinticincocent" readonly></td>
						<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'monedas','','',0.25)></a></td></tr>";	}}?>
					</tr>
					<tr>
						<td><input type="text" class="textefectivo" id="cincuentacent" value=0.50 readonly></td>
						<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==0.50){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="cincuentacentval" onchange=calcular(this,'<?php echo($numero); ?>',"monedas"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
						<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalcincuentacent" readonly></td>
						<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'monedas','','',0.50)></a></td></tr>";	}}?>
					</tr>
					<tr>
						<td><input type="text" class="textefectivo" id="ciencent" value=1.00 readonly></td>
						<td><input type="number" class="textefectivo" value="<?php $val=0; foreach($monedas as $moneda){if($moneda[UnidxMoneda]==1.00){echo $moneda[Cantidad]; $total= ($moneda[UnidxMoneda])*($moneda[Cantidad]); $val=1;}} ?>" id="ciencentval" onchange=calcular(this,'<?php echo($numero); ?>',"monedas"); <?php if($val==1 || $guias[0][Estado] == 'Pasivo'){echo 'readonly';} ?>></td>
						<td><input type="text" class="textefectivo" value="<?php if($val!=0){echo $total;}?>" id="totalciencent" readonly></td>
						<?php if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){ if ($val!=0){ echo "<td ><a href='#' id='Eliminar' class='Boton_Eliminar icon_eliminar' onclick=eliminaregistro('','',$numero,'monedas','','',1)></a></td></tr>";}}?>
					</tr>
				</table>
		</div><hr>
		<?php 
		echo "</div></div>";
		echo '</div>';
		//--------------------------------------------------------
		// lineas  para presentar el valor en formato de numero con 2 decimales
		$totalcobranza=number_format($totbillete+$totmonedas+$totcheque+$tottransferencia+$tottarjeta+$totret, 2, '.', '');
		$totfacturas = number_format($totefectivo+$totcheque+$tottransferencia+$tottarjeta+$totret, 2, '.', '');
		$diferencia = $totfacturas - $totalcobranza;
		$diferencia = number_format($diferencia, 2, '.', '');
		//**********************************************
		//TOTALES DE LA GUIA ---------
		echo "<div class='menu_acordeon'>";
			echo "<div class='titulo'>";
				echo "<p class='tutulo'>Totales  - <label class='totalmenu'>Diferencia: $ $diferencia</label></p>";
			echo "</div>";
		echo "<div class='info'>";
			echo '<div id="totales"><table rules="all" border=1>';
				echo '<tr><td><b>Total Facturas: $</td><td><input type="text" id="totfacturas" class="textefectivo" value=' . $totfacturas . ' readonly></td></tr>';
				echo '<tr style="color:red;"><td><b>Diferencia: $</td><td><input type="text" id="diferencia" class="textefectivo" style="color:red;" value=' . $diferencia . ' readonly></td></tr>';
				echo '<tr><td><b>Total Recaudacion: $</td><td><b><input type="text" id="totalcobranza" class="textefectivo" value=' . $totalcobranza . ' readonly></td>';
				echo '<br></table></div>';
				echo '<hr class="linea">';
		echo '</div></div>';
		echo '<input type="hidden" id="totmonedas" class="textefectivo" value=' . $totmonedas . ' readonly>';
		echo '<td><input type="hidden" id="totbilletes" class="textefectivo" value=' . $totbillete . ' readonly >';
		echo '<input type="hidden" id="totefectivo" class="textefectivo" value=' . $totefectivo . ' readonly>';
		echo '<input type="hidden" id="totcheques" class="textefectivo" value=' . $totcheque . ' readonly>';
		echo '<input type="hidden" id="totTransf" class="textefectivo" value=' . $tottransferencia . ' readonly>';
		echo '<input type="hidden" id="totTarjetas" class="textefectivo" value=' . $tottarjeta . ' readonly>';
		echo '<input type="hidden" id="totRet" class="textefectivo" value=' . $totret . ' readonly>';
		
		//**********************************
		if($guias[0][Estado]==Actualizada  || $guias[0][Estado]==Pendiente || $guias[0][Estado]==Movil){
			$validacheque =  $cone ->validacheque($numero, $local);
			$a = 0;
			foreach ($validacheque as $value) {
				if($value[banco]==null || $value[banco]=='---' || $value[Cheque]==null || $value[cuenta]==null ){
					$a++;
				};
			}
			$tr= 0;
			$validaTransf =  $cone ->transacciones($numero,'TR',$local);
			foreach ($validaTransf as $value) {
				if($value[documento]=='' || $value[banco]=='---' || $value[fechaTransferencia]==null ){
					$tr++;
				};
			}
			$tc= 0;
			$validaTarjeta =  $cone ->transacciones($numero,'TC',$local);
			foreach ($validaTarjeta as $value) {
				if($value[documento]=='' || $value[autorizacion]==null || $value[banco]=='---' || $value[numTarjeta]==null || $value[tarjeta]==null ){
					$tc++;
				};
			}
			$rt = 0;
			$validaRet =  $cone ->retenciones($numero,$local);
			foreach ($validaRet as $value) {
				if(($value[numDocumento]==0 || $value[numDocumento]==null) || $value[rubro]==null ){
					$rt++;
				};
			}
			echo '<br><a href="#" id="act_guia" class="Boton_Guardar" onclick=GuiaCobro(' . $numero . ');> Rec&aacute;lculo</a>';
			echo '<center><br><a href="#" id="guardarguia" class="boton_guardar icon_guardar" onclick=guardarGuia(' . $a . ',' . $tr . ',' . $tc . ',' . $rt . ',' . $diferencia . ',' . $numero . ',' . $cone -> ValidaGuia0($numero, $local) . ');> Grabar Guia</a></center>';
		}
	}else { echo '<div id="noexiste"><h1>La guia n&uacute;mero ' . $numero . ' no Existe</div>';}

?>
</div></body>
</html>