<?php 
	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

	$empresa = Config::$Empresa;
	$local = $_SESSION['log_LOCAL'];
	$cajaP = Config::$Caja;

	//VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
	$ver->VerificarSCAIIN('index.php',$local);
	$usuario = $_SESSION['log_IDUSUARIO'];
	$idEmpleado = $_SESSION['log_IDEMPLEADO'];

	$tipo = $_POST['tipo'];
	
	switch ($tipo) {
		case 'cabecera':
			$numProforma = filter_input(INPUT_POST,'numero');
			$fecha = filter_input(INPUT_POST,'fecha');
			$direccion = filter_input(INPUT_POST,'direccion');
			$email = filter_input(INPUT_POST,'email');
			$vendedor = filter_input(INPUT_POST,'vendedor');
			$asesor = filter_input(INPUT_POST,'asesor');
			$formaPago = filter_input(INPUT_POST,'formaPago');
			$diasPlazo = filter_input(INPUT_POST,'diasPlazo');
			$canalCom = filter_input(INPUT_POST,'canalCom');
			$observacion = filter_input(INPUT_POST,'observacion');

			$datos = "Fecha='$fecha',Dirección='$direccion',Email='$email',Contacto='$vendedor',Empleado_Asesor='$asesor',Forma_Pago='$formaPago',
					Plazo_Días='$diasPlazo',CanalCom='$canalCom',Observación='$observacion', ID_Usuario_E='$usuario',Edicion=GETDATE()";
			echo $cone->editarDatosProforma($tipo, $datos, $local, $numProforma);

			break;
		case 'cliente':
			$numProforma = filter_input(INPUT_POST,'numero');
			$direccion = filter_input(INPUT_POST,'direccion');
			$codCli = filter_input(INPUT_POST,'codigo');
			$nomCliente = filter_input(INPUT_POST,'nombre');
			$cliente = filter_input(INPUT_POST,'cliente');
			$ruc = filter_input(INPUT_POST,'ruc');
			if($cliente == ''){
				$cliente = $nomCliente;
			}

			$datos = "Cliente_act='$codCli',Cliente='$nomCliente',Nombre='$cliente',RUC='$ruc',Dirección='$direccion'";

			echo $cone->editarDatosProforma($tipo, $datos, $local, $numProforma);
			break;

		case 'servicioDetalle':
			$numProforma = filter_input(INPUT_POST,'numProforma');
			$enteras = filter_input(INPUT_POST,'cantEnteras');
			$unidades = filter_input(INPUT_POST,'cantUnidades');
			$descuento = filter_input(INPUT_POST,'dscto');
			$valUnidad  = filter_input(INPUT_POST,'valUnidad');
			$valEntera = filter_input(INPUT_POST,'valEntera');
			$iva = filter_input(INPUT_POST,'iva');
			$idProducto = filter_input(INPUT_POST,'idProducto');

			$datos = "Enteras=$enteras, Unidades=$unidades, Precio_Unidad=$valUnidad, Dscto=$descuento, Precio_Entera=$valEntera, precio_real=$iva";
			$condicion = " where Número=$numProforma and Local =$local and Producto='$idProducto' and ficha=0";
			echo $cone->editarDetalleProforma($tipo, $datos, $condicion);

			break;
		case 'produccionDetalle':
			$numProforma = filter_input(INPUT_POST,'numProforma');
			$enteras = filter_input(INPUT_POST,'cantEnteras');
			$unidades = filter_input(INPUT_POST,'cantUnidades');
			$descuento = filter_input(INPUT_POST,'dscto');
			$valUnidad  = filter_input(INPUT_POST,'valUnidad');
			$valEntera = filter_input(INPUT_POST,'valEntera');
			$iva = filter_input(INPUT_POST,'iva');
			$idProducto = filter_input(INPUT_POST,'idProducto');

			$datos = "Enteras=$enteras, Unidades=$unidades, Precio_Unidad=$valUnidad, Dscto=$descuento, Precio_Entera=$valEntera, precio_real=$iva";
			$condicion = " where Número=$numProforma and Local =$local and Producto='$idProducto' and ficha=0";
			echo $cone->editarDetalleProforma($tipo, $datos, $condicion);
			
			break;
		case 'totales':
			$numProforma = filter_input(INPUT_POST,'numProforma');
			$subTotalFicha = filter_input(INPUT_POST,'sTotFicha');
			$ivaFicha = filter_input(INPUT_POST,'IFicha');
			$subTotalPrd = filter_input(INPUT_POST,'totPRD');
			$ivaPrd  = filter_input(INPUT_POST,'iPRD');
			$subTotalSrv  = filter_input(INPUT_POST,'totSRV');
			$ivaSRV = filter_input(INPUT_POST,'iSRV');

			$totalBrutoPrd = $subTotalPrd + $subTotalSrv;
			$TotalIvaPrd = $ivaPrd + $ivaSRV;
			$subTotalBruto = $totalBrutoPrd + $subTotalFicha;
			$totalIva = number_format($TotalIvaPrd,2)  + number_format($ivaFicha,2);
			$totalIva = $subTotalBruto * 0.12;
			
			$datos = "TotalBruto=$subTotalBruto, TotalIva=$totalIva, TotalBrutoPrd=$totalBrutoPrd, totalIvaPrd=$TotalIvaPrd, TotalBrutoFicha=$subTotalFicha, TotalIvaFicha=$ivaFicha";
			echo $cone->editarTotalesProforma($datos,$local,$numProforma);

			break;
			
		case 'servicio':
			$datos = "'$empresa','$local','$numProforma','$idProducto','$enteras','$unidades','$precioUnidad','$descuento','$descAdicional','$iva','$precioEntera',
					'$precioReal',0,'$detalle',NULL,'$unidadxEntera'";

			echo $cone->editarDatosProforma($tipo, $datos,$local,$numProforma);
			break;
		case 'produccion':
			
			break;
	}
?>