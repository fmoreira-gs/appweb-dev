<?php 

	//ARCHIVOS DE CONFIGURACION GLOBAL
	require_once __DIR__ . '../../app/Config.php';
	require_once __DIR__ . '../../app/Model.php';

    $local = $_SESSION['log_LOCAL'];
    //VERIFICAR SI ESTA AUTENTICADO
    $ver = new Login();
    $ver->VerificarSCAIIN('index.php',$local);

    $numProforma = $_GET['numProforma'];
    $tipo = $_GET['tipo'];
    $detalles = explode(",",$_GET['detalle']);
    
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo(Config::$mvc_titulo); echo (Config::$mvc_scain); ?></title>
        <!-- Bootstrap core CSS -->    
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/select2.css">
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="js/script.js"></script> 
        <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="../js/select2.js"></script>
    </head>
    <body>
        <?php
            $aux=1;
            foreach ($detalles as $row) {
                echo '<input type="hidden" value="' .  str_replace('@',' ', $row) . '" id="txtidProductoN' . $aux . '" />';
                $aux++;
            }
            echo '<input type="hidden" value="' . $aux . '" id="txtContador" />';
        ?>
        <div class="table-responsive">
            <form>
                <center><b><p class="mensaje">BUSQUEDA DE PRODUCTOS</p></b></center>
                <input type="radio" name="opcion" id="R_codigo" value="CODIGO" style="width: 5%; height:5%;" > C&Oacute;DIGO&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="opcion" id="R_nombre" value="NOMBRE" style="width: 5%; height:5%;" checked > NOMBRE
                <br>
                <input class="form-control" type="text" name="criterio" id="criterio" ><br>
                <div>
                    <?php
                    echo'
                        <center><button type="button" class="btn btn-inline btn-success" id="buscar" onClick=BusquedaProductos(\'' . $tipo . '\',' . $numProforma . ')><i class="fa fa-wpexplorer"></i> Buscar</button>
                    ';
                    ?>
                <button type="button" class="btn btn-primary" onClick=cierraVentana()><i class="fa fa-reply"></i> Cancelar</button></center>
                </div>
            <div id = "buscaparametro"><br> 
            </div>
            </form>
        </div>
    </body>
</html>